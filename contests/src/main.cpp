#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>
#include <array>
#include <algorithm>
#include <numeric>
#include <random>
#include <chrono>
#include <tuple>
#include <cstdint>
#include <omp.h>
#include <immintrin.h>

//----------------------------------------------------------------------------
// Problem data
//----------------------------------------------------------------------------
class Problem {

public:
	using Block = std::array<int, 3>;

private:
	uint32_t m_random_x;
	int m_timeout;
	int m_numbers;
	int m_height;
	int m_width;
	std::vector<Block> m_blocks;

	uint32_t next_random() noexcept {
		const uint32_t a = 1103515245u, c = 12345u;
		m_random_x = (a * m_random_x + c);
		return (m_random_x >> 16) & 0x7fff;
	}

	std::vector<Block> generate_blocks(uint32_t n){
		std::vector<Block> blocks;
		for(uint32_t i = 0; i < n; ++i){
			const uint32_t digits = next_random() % 1000;
			blocks.push_back(Block{
				static_cast<int>(digits % 10),
				static_cast<int>(digits / 10 % 10),
				static_cast<int>(digits / 100)
			});
		}
		return blocks;
	}

public:
	Problem(uint32_t seed = 0)
		: m_random_x(seed)
		, m_timeout(next_random() % 196 + 5)
		, m_numbers(next_random() % 1901 + 100)
		, m_height(next_random() % 81 + 20)
		, m_width(next_random() % 181 + 20)
		, m_blocks()
	{
		if(seed == 0){
			m_timeout = 5;
			m_numbers = 100;
			m_height = 20;
			m_width = 20;
		}
		m_blocks = generate_blocks(m_numbers);
	}

	int timeout() const noexcept { return m_timeout; }
	int numbers() const noexcept { return m_numbers; }
	int height() const noexcept { return m_height; }
	int width() const noexcept { return m_width; }
	const std::vector<Block> &blocks() const noexcept { return m_blocks; }

};

//----------------------------------------------------------------------------
// Multiprecision computation
//----------------------------------------------------------------------------
static const int MAX_PRECISION = 12;

template <unsigned long INT_PRECISION>
inline void mpint_add(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1,
	uint64_t carry = 0)
{
}

#define MOV_ADC(offset) \
	"mov  " #offset "(%2), %%rax \n" \
	"adc  " #offset "(%3), %%rax \n" \
	"mov  %%rax, " #offset "(%1) \n"

template <>
inline void mpint_add<1ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	dst[0] = src0[0] + src1[0] + carry;
}

template <>
inline void mpint_add<2ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<3ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<4ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<5ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		MOV_ADC(0x20)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<6ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		MOV_ADC(0x20) MOV_ADC(0x28)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<7ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		MOV_ADC(0x20) MOV_ADC(0x28) MOV_ADC(0x30)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<8ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		MOV_ADC(0x20) MOV_ADC(0x28) MOV_ADC(0x30) MOV_ADC(0x38)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<9ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		MOV_ADC(0x20) MOV_ADC(0x28) MOV_ADC(0x30) MOV_ADC(0x38)
		MOV_ADC(0x40)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<10ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		MOV_ADC(0x20) MOV_ADC(0x28) MOV_ADC(0x30) MOV_ADC(0x38)
		MOV_ADC(0x40) MOV_ADC(0x48)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<11ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		MOV_ADC(0x20) MOV_ADC(0x28) MOV_ADC(0x30) MOV_ADC(0x38)
		MOV_ADC(0x40) MOV_ADC(0x48) MOV_ADC(0x50)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

template <>
inline void mpint_add<12ul>(
	uint64_t *dst, const uint64_t *src0, const uint64_t *src1, uint64_t carry)
{
	__asm__ volatile (
		"bt   $0, %4 \n"
		MOV_ADC()     MOV_ADC(0x08) MOV_ADC(0x10) MOV_ADC(0x18)
		MOV_ADC(0x20) MOV_ADC(0x28) MOV_ADC(0x30) MOV_ADC(0x38)
		MOV_ADC(0x40) MOV_ADC(0x48) MOV_ADC(0x50) MOV_ADC(0x58)
		: "=m"(dst)
		: "r"(dst), "r"(src0), "r"(src1), "r"(carry)
		: "%rax", "cc", "memory");
}

#undef MOV_ADC

template <unsigned long INT_PRECISION>
inline void mpint_inplace_srl(uint64_t *x, int shift){
	const __m128i v_lshift = _mm_cvtsi32_si128(64 - shift);
	const __m128i v_rshift = _mm_cvtsi32_si128(shift);
	__m128i borrow = _mm_setzero_si128();
	for(int i = (INT_PRECISION - 1) & ~1; i >= 0; i -= 2){
		const __m128i lo = _mm_loadu_si128(
			reinterpret_cast<const __m128i *>(x + i));
		const __m128i hi = _mm_alignr_epi8(borrow, lo, 8);
		const __m128i y = _mm_or_si128(
			_mm_srl_epi64(lo, v_rshift),
			_mm_sll_epi64(hi, v_lshift));
		_mm_storeu_si128(reinterpret_cast<__m128i *>(x + i), y);
		borrow = lo;
	}
}

//----------------------------------------------------------------------------
// Collatz problem
//----------------------------------------------------------------------------
template <unsigned long CURRENT_PRECISION>
inline int collatz_length_impl(uint64_t *x, uint64_t *work){
	int answer = 0;
	while(x[CURRENT_PRECISION - 2] > 0){
		if(x[0] & 1){
			mpint_add<CURRENT_PRECISION>(work, x, x);
			mpint_add<CURRENT_PRECISION>(x, x, work, 1);
			++answer;
		}
		if(x[0] == 0){
			for(int i = 1; i < CURRENT_PRECISION; ++i){ x[i - 1] = x[i]; }
			answer += 64;
		}else{
			const int shift = __builtin_ctzll(x[0]);
			mpint_inplace_srl<CURRENT_PRECISION>(x, shift);
			answer += shift;
		}
	}
	answer += collatz_length_impl<CURRENT_PRECISION - 1>(x, work);
	return answer;
}

// (r8, r9, r10, r11, r12, r13, r14, r15) =
//   (x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7])
#define COLLATZ_IMPL_ASSEMBLY_8(label) \
	#label "_length_8_loop%=:            \n" \
	"    mov  %%r14, %%rax               \n" \
	"    shr  $32,   %%rax               \n" \
	"    or   %%r15, %%rax               \n" \
	"    jz   " #label "_length_7_loop%= \n" \
	"    bt   $0, %%r8                   \n" \
	"    jnc  " #label "_length_8_even%= \n" \
	/* x = 3x + 1 */ \
	"    clc                               \n" \
	"    mov     %%r8,  %%rax              \n" \
	"    mov     %%r9,  %%rcx              \n" \
	"    adc     %%r8,  %%rax              \n" \
	"    adc     %%r9,  %%rcx              \n" \
	"    movq    %%rax, %%xmm0             \n" \
	"    vpinsrq $1, %%rcx, %%xmm0, %%xmm0 \n" \
	"    mov     %%r10, %%rax              \n" \
	"    mov     %%r11, %%rcx              \n" \
	"    adc     %%r10, %%rax              \n" \
	"    adc     %%r11, %%rcx              \n" \
	"    movq    %%rax, %%xmm1             \n" \
	"    vpinsrq $1, %%rcx, %%xmm1, %%xmm1 \n" \
	"    mov     %%r12, %%rax              \n" \
	"    mov     %%r13, %%rcx              \n" \
	"    adc     %%r12, %%rax              \n" \
	"    adc     %%r13, %%rcx              \n" \
	"    movq    %%rax, %%xmm2             \n" \
	"    vpinsrq $1, %%rcx, %%xmm2, %%xmm2 \n" \
	"    mov     %%r14, %%rax              \n" \
	"    mov     %%r15, %%rcx              \n" \
	"    adc     %%r14, %%rax              \n" \
	"    adc     %%r15, %%rcx              \n" \
	"    movq    %%rax, %%xmm3             \n" \
	"    vpinsrq $1, %%rcx, %%xmm3, %%xmm3 \n" \
	"    stc                               \n" \
	"    movq    %%xmm0, %%rax             \n" \
	"    vpextrq $1, %%xmm0, %%rcx         \n" \
	"    adc     %%rax, %%r8               \n" \
	"    adc     %%rcx, %%r9               \n" \
	"    movq    %%xmm1, %%rax             \n" \
	"    vpextrq $1, %%xmm1, %%rcx         \n" \
	"    adc     %%rax, %%r10              \n" \
	"    adc     %%rcx, %%r11              \n" \
	"    movq    %%xmm2, %%rax             \n" \
	"    vpextrq $1, %%xmm2, %%rcx         \n" \
	"    adc     %%rax, %%r12              \n" \
	"    adc     %%rcx, %%r13              \n" \
	"    movq    %%xmm3, %%rax             \n" \
	"    vpextrq $1, %%xmm3, %%rcx         \n" \
	"    adc     %%rax, %%r14              \n" \
	"    adc     %%rcx, %%r15              \n" \
	"    add     $1, %0                    \n" \
	/* x = x >> ctz(x) */ \
	#label "_length_8_even%=:              \n" \
	"    bsf  %%r8, %%rcx                  \n" \
	"    jz   " #label "_length_8_shift%=  \n" \
	"    shrd %%cl, %%r9,  %%r8            \n" \
	"    shrd %%cl, %%r10, %%r9            \n" \
	"    shrd %%cl, %%r11, %%r10           \n" \
	"    shrd %%cl, %%r12, %%r11           \n" \
	"    shrd %%cl, %%r13, %%r12           \n" \
	"    shrd %%cl, %%r14, %%r13           \n" \
	"    shrd %%cl, %%r15, %%r14           \n" \
	"    shr  %%cl, %%r15                  \n" \
	"    add  %%rcx, %0                    \n" \
	"    jmp  " #label "_length_8_loop%=   \n" \
	#label "_length_8_shift%=:             \n" \
	"    mov  %%r9,  %%r8                  \n" \
	"    mov  %%r10, %%r9                  \n" \
	"    mov  %%r11, %%r10                 \n" \
	"    mov  %%r12, %%r11                 \n" \
	"    mov  %%r13, %%r12                 \n" \
	"    mov  %%r14, %%r13                 \n" \
	"    mov  %%r15, %%r14                 \n" \
	"    xor  %%r15, %%r15                 \n" \
	"    add  $64, %0                      \n" \
	"    jmp  " #label "_length_8_loop%=   \n"

// (r8, r9, r10, r11, r12, r13, r14) =
//   (x[0], x[1], x[2], x[3], x[4], x[5], x[6])
#define COLLATZ_IMPL_ASSEMBLY_7(label) \
	#label "_length_7_loop%=:            \n" \
	"    mov  %%r13, %%rax               \n" \
	"    shr  $32,   %%rax               \n" \
	"    or   %%r14, %%rax               \n" \
	"    jz   " #label "_length_6_loop%= \n" \
	"    bt   $0, %%r8                   \n" \
	"    jnc  " #label "_length_7_even%= \n" \
	/* x = 3x + 1 */ \
	"    clc                               \n" \
	"    mov     %%r8,  %%rax              \n" \
	"    mov     %%r9,  %%rcx              \n" \
	"    adc     %%r8,  %%rax              \n" \
	"    adc     %%r9,  %%rcx              \n" \
	"    movq    %%rax, %%xmm0             \n" \
	"    vpinsrq $1, %%rcx, %%xmm0, %%xmm0 \n" \
	"    mov     %%r10, %%rax              \n" \
	"    mov     %%r11, %%rcx              \n" \
	"    adc     %%r10, %%rax              \n" \
	"    adc     %%r11, %%rcx              \n" \
	"    movq    %%rax, %%xmm1             \n" \
	"    vpinsrq $1, %%rcx, %%xmm1, %%xmm1 \n" \
	"    mov     %%r12, %%rax              \n" \
	"    mov     %%r13, %%rcx              \n" \
	"    adc     %%r12, %%rax              \n" \
	"    adc     %%r13, %%rcx              \n" \
	"    movq    %%rax, %%xmm2             \n" \
	"    vpinsrq $1, %%rcx, %%xmm2, %%xmm2 \n" \
	"    mov     %%r14, %%rax              \n" \
	"    adc     %%r14, %%rax              \n" \
	"    movq    %%rax, %%xmm3             \n" \
	"    stc                               \n" \
	"    movq    %%xmm0, %%rax             \n" \
	"    vpextrq $1, %%xmm0, %%rcx         \n" \
	"    adc     %%rax, %%r8               \n" \
	"    adc     %%rcx, %%r9               \n" \
	"    movq    %%xmm1, %%rax             \n" \
	"    vpextrq $1, %%xmm1, %%rcx         \n" \
	"    adc     %%rax, %%r10              \n" \
	"    adc     %%rcx, %%r11              \n" \
	"    movq    %%xmm2, %%rax             \n" \
	"    vpextrq $1, %%xmm2, %%rcx         \n" \
	"    adc     %%rax, %%r12              \n" \
	"    adc     %%rcx, %%r13              \n" \
	"    movq    %%xmm3, %%rax             \n" \
	"    adc     %%rax, %%r14              \n" \
	"    add     $1, %0                    \n" \
	/* x = x >> ctz(x) */ \
	#label "_length_7_even%=:              \n" \
	"    bsf  %%r8, %%rcx                  \n" \
	"    jz   " #label "_length_7_shift%=  \n" \
	"    shrd %%cl, %%r9,  %%r8            \n" \
	"    shrd %%cl, %%r10, %%r9            \n" \
	"    shrd %%cl, %%r11, %%r10           \n" \
	"    shrd %%cl, %%r12, %%r11           \n" \
	"    shrd %%cl, %%r13, %%r12           \n" \
	"    shrd %%cl, %%r14, %%r13           \n" \
	"    shr  %%cl, %%r14                  \n" \
	"    add  %%rcx, %0                    \n" \
	"    jmp  " #label "_length_7_loop%=   \n" \
	#label "_length_7_shift%=:             \n" \
	"    mov  %%r9,  %%r8                  \n" \
	"    mov  %%r10, %%r9                  \n" \
	"    mov  %%r11, %%r10                 \n" \
	"    mov  %%r12, %%r11                 \n" \
	"    mov  %%r13, %%r12                 \n" \
	"    mov  %%r14, %%r13                 \n" \
	"    xor  %%r14, %%r14                 \n" \
	"    add  $64, %0                      \n" \
	"    jmp  " #label "_length_7_loop%=   \n"

// (r8, r9, r10, r11, r12, r13) = (x[0], x[1], x[2], x[3], x[4], x[5])
#define COLLATZ_IMPL_ASSEMBLY_6(label) \
	#label "_length_6_loop%=:            \n" \
	"    mov  %%r12, %%r15               \n" \
	"    shr  $32,   %%r15               \n" \
	"    or   %%r13, %%r15               \n" \
	"    jz   " #label "_length_5_loop%= \n" \
	"    bt   $0, %%r8                   \n" \
	"    jnc  " #label "_length_6_even%= \n" \
	/* x = 3x + 1 */ \
	"    clc                               \n" \
	"    mov     %%r8,  %%r14              \n" \
	"    mov     %%r9,  %%r15              \n" \
	"    adc     %%r8,  %%r14              \n" \
	"    adc     %%r9,  %%r15              \n" \
	"    movq    %%r14, %%xmm0             \n" \
	"    vpinsrq $1, %%r15, %%xmm0, %%xmm0 \n" \
	"    mov     %%r10, %%r14              \n" \
	"    mov     %%r11, %%r15              \n" \
	"    adc     %%r10, %%r14              \n" \
	"    adc     %%r11, %%r15              \n" \
	"    movq    %%r14, %%xmm1             \n" \
	"    vpinsrq $1, %%r15, %%xmm1, %%xmm1 \n" \
	"    mov     %%r12, %%r14              \n" \
	"    mov     %%r13, %%r15              \n" \
	"    adc     %%r12, %%r14              \n" \
	"    adc     %%r13, %%r15              \n" \
	"    movq    %%r14, %%xmm2             \n" \
	"    vpinsrq $1, %%r15, %%xmm2, %%xmm2 \n" \
	"    stc                               \n" \
	"    movq    %%xmm0, %%r14             \n" \
	"    vpextrq $1, %%xmm0, %%r15         \n" \
	"    adc     %%r14, %%r8               \n" \
	"    adc     %%r15, %%r9               \n" \
	"    movq    %%xmm1, %%r14             \n" \
	"    vpextrq $1, %%xmm1, %%r15         \n" \
	"    adc     %%r14, %%r10              \n" \
	"    adc     %%r15, %%r11              \n" \
	"    movq    %%xmm2, %%r14             \n" \
	"    vpextrq $1, %%xmm2, %%r15         \n" \
	"    adc     %%r14, %%r12              \n" \
	"    adc     %%r15, %%r13              \n" \
	"    add     $1, %0                    \n" \
	/* x = x >> ctz(x) */ \
	#label "_length_6_even%=:              \n" \
	"    bsf  %%r8, %%rcx                  \n" \
	"    jz   " #label "_length_6_shift%=  \n" \
	"    shrd %%cl, %%r9,  %%r8            \n" \
	"    shrd %%cl, %%r10, %%r9            \n" \
	"    shrd %%cl, %%r11, %%r10           \n" \
	"    shrd %%cl, %%r12, %%r11           \n" \
	"    shrd %%cl, %%r13, %%r12           \n" \
	"    shr  %%cl, %%r13                  \n" \
	"    add  %%rcx, %0                    \n" \
	"    jmp  " #label "_length_6_loop%=   \n" \
	#label "_length_6_shift%=:             \n" \
	"    mov  %%r9,  %%r8                  \n" \
	"    mov  %%r10, %%r9                  \n" \
	"    mov  %%r11, %%r10                 \n" \
	"    mov  %%r12, %%r11                 \n" \
	"    mov  %%r13, %%r12                 \n" \
	"    xor  %%r13, %%r13                 \n" \
	"    add  $64, %0                      \n" \
	"    jmp  " #label "_length_6_loop%=   \n"

// (r8, r9, r10, r11, r12) = (x[0], x[1], x[2], x[3], x[4])
#define COLLATZ_IMPL_ASSEMBLY_5(label) \
	#label "_length_5_loop%=:            \n" \
	"    mov  %%r11, %%r15               \n" \
	"    shr  $32,   %%r15               \n" \
	"    or   %%r12, %%r15               \n" \
	"    jz   " #label "_length_4_loop%= \n" \
	"    bt   $0, %%r8                   \n" \
	"    jnc  " #label "_length_5_even%= \n" \
	/* x = 3x + 1 */ \
	"    clc                               \n" \
	"    mov     %%r8,  %%r14              \n" \
	"    mov     %%r9,  %%r15              \n" \
	"    adc     %%r8,  %%r14              \n" \
	"    adc     %%r9,  %%r15              \n" \
	"    movq    %%r14, %%xmm0             \n" \
	"    vpinsrq $1, %%r15, %%xmm0, %%xmm0 \n" \
	"    mov     %%r10, %%r14              \n" \
	"    mov     %%r11, %%r15              \n" \
	"    adc     %%r10, %%r14              \n" \
	"    adc     %%r11, %%r15              \n" \
	"    movq    %%r14, %%xmm1             \n" \
	"    vpinsrq $1, %%r15, %%xmm1, %%xmm1 \n" \
	"    mov     %%r12, %%r14              \n" \
	"    adc     %%r12, %%r14              \n" \
	"    movq    %%r14, %%xmm2             \n" \
	"    stc                               \n" \
	"    movq    %%xmm0, %%r14             \n" \
	"    vpextrq $1, %%xmm0, %%r15         \n" \
	"    adc     %%r14, %%r8               \n" \
	"    adc     %%r15, %%r9               \n" \
	"    movq    %%xmm1, %%r14             \n" \
	"    vpextrq $1, %%xmm1, %%r15         \n" \
	"    adc     %%r14, %%r10              \n" \
	"    adc     %%r15, %%r11              \n" \
	"    movq    %%xmm2, %%r14             \n" \
	"    adc     %%r14, %%r12              \n" \
	"    add     $1, %0                    \n" \
	/* x = x >> ctz(x) */ \
	#label "_length_5_even%=:              \n" \
	"    bsf  %%r8, %%rcx                  \n" \
	"    jz   " #label "_length_5_shift%=  \n" \
	"    shrd %%cl, %%r9,  %%r8            \n" \
	"    shrd %%cl, %%r10, %%r9            \n" \
	"    shrd %%cl, %%r11, %%r10           \n" \
	"    shrd %%cl, %%r12, %%r11           \n" \
	"    shr  %%cl, %%r12                  \n" \
	"    add  %%rcx, %0                    \n" \
	"    jmp  " #label "_length_5_loop%=   \n" \
	#label "_length_5_shift%=:             \n" \
	"    mov  %%r9,  %%r8                  \n" \
	"    mov  %%r10, %%r9                  \n" \
	"    mov  %%r11, %%r10                 \n" \
	"    mov  %%r12, %%r11                 \n" \
	"    xor  %%r12, %%r12                 \n" \
	"    add  $64, %0                      \n" \
	"    jmp  " #label "_length_5_loop%=   \n"

// (r8, r9, r10, r11) = (x[0], x[1], x[2], x[3])
#define COLLATZ_IMPL_ASSEMBLY_4(label) \
	#label "_length_4_loop%=:            \n" \
	"    mov  %%r10, %%r15               \n" \
	"    shr  $32,   %%r15               \n" \
	"    or   %%r11, %%r15               \n" \
	"    jz   " #label "_length_3_loop%= \n" \
	"    bt   $0, %%r8                   \n" \
	"    jnc  " #label "_length_4_even%= \n" \
	/* x = 3x + 1 */ \
	"    mov  %%r8,  %%r12 \n" \
	"    mov  %%r9,  %%r13 \n" \
	"    mov  %%r10, %%r14 \n" \
	"    mov  %%r11, %%r15 \n" \
	"    clc               \n" \
	"    adc  %%r8,  %%r12 \n" \
	"    adc  %%r9,  %%r13 \n" \
	"    adc  %%r10, %%r14 \n" \
	"    adc  %%r11, %%r15 \n" \
	"    stc               \n" \
	"    adc  %%r12, %%r8  \n" \
	"    adc  %%r13, %%r9  \n" \
	"    adc  %%r14, %%r10 \n" \
	"    adc  %%r15, %%r11 \n" \
	"    add  $1, %0       \n" \
	/* x = x >> ctz(x) */ \
	#label "_length_4_even%=:              \n" \
	"    bsf  %%r8, %%rcx                  \n" \
	"    jz   " #label "_length_4_shift%=  \n" \
	"    shrd %%cl, %%r9,  %%r8            \n" \
	"    shrd %%cl, %%r10, %%r9            \n" \
	"    shrd %%cl, %%r11, %%r10           \n" \
	"    shr  %%cl, %%r11                  \n" \
	"    add  %%rcx, %0                    \n" \
	"    jmp  " #label "_length_4_loop%=   \n" \
	#label "_length_4_shift%=:             \n" \
	"    mov  %%r9,  %%r8                  \n" \
	"    mov  %%r10, %%r9                  \n" \
	"    mov  %%r11, %%r10                 \n" \
	"    xor  %%r11, %%r11                 \n" \
	"    add  $64, %0                      \n" \
	"    jmp  " #label "_length_4_loop%=   \n"

// (r8, r9, r10) = (x[0], x[1], x[2])
#define COLLATZ_IMPL_ASSEMBLY_3(label) \
	#label "_length_3_loop%=:            \n" \
	"    mov  %%r9,  %%r15               \n" \
	"    shr  $32,   %%r15               \n" \
	"    or   %%r10, %%r15               \n" \
	"    jz   " #label "_length_2_loop%= \n" \
	"    bt   $0, %%r8                   \n" \
	"    jnc  " #label "_length_3_even%= \n" \
	/* x = 3x + 1 */ \
	"    mov  %%r8,  %%r12 \n" \
	"    mov  %%r9,  %%r13 \n" \
	"    mov  %%r10, %%r14 \n" \
	"    clc               \n" \
	"    adc  %%r8,  %%r12 \n" \
	"    adc  %%r9,  %%r13 \n" \
	"    adc  %%r10, %%r14 \n" \
	"    stc               \n" \
	"    adc  %%r12, %%r8  \n" \
	"    adc  %%r13, %%r9  \n" \
	"    adc  %%r14, %%r10 \n" \
	"    add  $1, %0       \n" \
	/* x = x >> ctz(x) */ \
	#label "_length_3_even%=:              \n" \
	"    bsf  %%r8, %%rcx                  \n" \
	"    jz   " #label "_length_3_shift%=  \n" \
	"    shrd %%cl, %%r9,  %%r8            \n" \
	"    shrd %%cl, %%r10, %%r9            \n" \
	"    shr  %%cl, %%r10                  \n" \
	"    add  %%rcx, %0                    \n" \
	"    jmp  " #label "_length_3_loop%=   \n" \
	#label "_length_3_shift%=:             \n" \
	"    mov  %%r9,  %%r8                  \n" \
	"    mov  %%r10, %%r9                  \n" \
	"    xor  %%r10, %%r10                 \n" \
	"    add  $64, %0                      \n" \
	"    jmp  " #label "_length_3_loop%=   \n"

// (r8, r9) = (x[0], x[1])
#define COLLATZ_IMPL_ASSEMBLY_2(label) \
	#label "_length_2_loop%=:            \n" \
	"    mov  %%r8, %%r15                \n" \
	"    shr  $32,  %%r15                \n" \
	"    or   %%r9, %%r15                \n" \
	"    jz   " #label "_length_1_loop%= \n" \
	"    bt   $0, %%r8                   \n" \
	"    jnc  " #label "_length_2_even%= \n" \
	/* x = 3x + 1 */ \
	"    mov  %%r8, %%r12 \n" \
	"    mov  %%r9, %%r13 \n" \
	"    clc              \n" \
	"    adc  %%r8, %%r12 \n" \
	"    adc  %%r9, %%r13 \n" \
	"    stc              \n" \
	"    adc  %%r12, %%r8 \n" \
	"    adc  %%r13, %%r9 \n" \
	"    add  $1, %0      \n" \
	/* x = x >> ctz(x) */ \
	#label "_length_2_even%=:              \n" \
	"    bsf  %%r8, %%rcx                  \n" \
	"    jz   " #label "_length_2_shift%=  \n" \
	"    shrd %%cl, %%r9, %%r8             \n" \
	"    shr  %%cl, %%r9                   \n" \
	"    add  %%rcx, %0                    \n" \
	"    jmp  " #label "_length_2_loop%=   \n" \
	#label "_length_2_shift%=:             \n" \
	"    mov  %%r9, %%r8                   \n" \
	"    xor  %%r9, %%r9                   \n" \
	"    add  $64, %0                      \n" \
	"    jmp  " #label "_length_2_loop%=   \n" \
	/* Precision = 1 */ \
	#label "_length_1_loop%=:            \n" \
	"    cmp  $2, %%r8                   \n" \
	"    jc   " #label "_finish%=        \n" \
	"    bt   $0, %%r8                   \n" \
	"    jnc  " #label "_length_1_even%= \n" \
	/* x = 3x + 1 */ \
	"    mov  %%r8, %%r12 \n" \
	"    add  %%r8, %%r12 \n" \
	"    stc              \n" \
	"    adc  %%r12, %%r8 \n" \
	"    add  $1, %0      \n" \
	/* x = x >> ctz(x) */ \
	#label "_length_1_even%=:              \n" \
	"    bsf  %%r8, %%rcx                  \n" \
	"    shr  %%cl, %%r8                   \n" \
	"    add  %%rcx, %0                    \n" \
	"    jmp  " #label "_length_1_loop%=   \n" \
	#label "_finish%=:                     \n"

template <>
inline int collatz_length_impl<8ul>(uint64_t *x, uint64_t *){
	uint64_t answer = 0;
	__asm__ volatile (
		"    mov  %1, %%r8  \n"
		"    mov  %2, %%r9  \n"
		"    mov  %3, %%r10 \n"
		"    mov  %4, %%r11 \n"
		"    mov  %5, %%r12 \n"
		"    mov  %6, %%r13 \n"
		"    mov  %7, %%r14 \n"
		"    mov  %8, %%r15 \n"
		"    xor  %0, %0    \n"
		COLLATZ_IMPL_ASSEMBLY_8(collatz8)
		COLLATZ_IMPL_ASSEMBLY_7(collatz8)
		COLLATZ_IMPL_ASSEMBLY_6(collatz8)
		COLLATZ_IMPL_ASSEMBLY_5(collatz8)
		COLLATZ_IMPL_ASSEMBLY_4(collatz8)
		COLLATZ_IMPL_ASSEMBLY_3(collatz8)
		COLLATZ_IMPL_ASSEMBLY_2(collatz8)
		: "=r"(answer)
		: "m"(x[0]), "m"(x[1]), "m"(x[2]), "m"(x[3]),
		  "m"(x[4]), "m"(x[5]), "m"(x[6]), "m"(x[7])
		: "cc",   "rax",  "rcx",
		  "r8",   "r9",   "r10",  "r11",
		  "r12",  "r13",  "r14",  "r15",
		  "xmm0", "xmm1", "xmm2", "xmm3");
	return static_cast<int>(answer);
}

template <>
inline int collatz_length_impl<7ul>(uint64_t *x, uint64_t *){
	uint64_t answer = 0;
	__asm__ volatile (
		"    mov  %1, %%r8  \n"
		"    mov  %2, %%r9  \n"
		"    mov  %3, %%r10 \n"
		"    mov  %4, %%r11 \n"
		"    mov  %5, %%r12 \n"
		"    mov  %6, %%r13 \n"
		"    mov  %7, %%r14 \n"
		"    xor  %0, %0    \n"
		COLLATZ_IMPL_ASSEMBLY_7(collatz7)
		COLLATZ_IMPL_ASSEMBLY_6(collatz7)
		COLLATZ_IMPL_ASSEMBLY_5(collatz7)
		COLLATZ_IMPL_ASSEMBLY_4(collatz7)
		COLLATZ_IMPL_ASSEMBLY_3(collatz7)
		COLLATZ_IMPL_ASSEMBLY_2(collatz7)
		: "=r"(answer)
		: "m"(x[0]), "m"(x[1]), "m"(x[2]), "m"(x[3]),
		  "m"(x[4]), "m"(x[5]), "m"(x[6])
		: "cc",   "rax",  "rcx",
		  "r8",   "r9",   "r10",  "r11",
		  "r12",  "r13",  "r14",  "r15",
		  "xmm0", "xmm1", "xmm2", "xmm3");
	return static_cast<int>(answer);
}

template <>
inline int collatz_length_impl<6ul>(uint64_t *x, uint64_t *){
	uint64_t answer = 0;
	__asm__ volatile (
		"    mov  %1, %%r8  \n"
		"    mov  %2, %%r9  \n"
		"    mov  %3, %%r10 \n"
		"    mov  %4, %%r11 \n"
		"    mov  %5, %%r12 \n"
		"    mov  %6, %%r13 \n"
		"    xor  %0, %0    \n"
		COLLATZ_IMPL_ASSEMBLY_6(collatz6)
		COLLATZ_IMPL_ASSEMBLY_5(collatz6)
		COLLATZ_IMPL_ASSEMBLY_4(collatz6)
		COLLATZ_IMPL_ASSEMBLY_3(collatz6)
		COLLATZ_IMPL_ASSEMBLY_2(collatz6)
		: "=r"(answer)
		: "m"(x[0]), "m"(x[1]), "m"(x[2]), "m"(x[3]),
		  "m"(x[4]), "m"(x[5])
		: "cc",   "rcx",
		  "r8",   "r9",   "r10",  "r11",
		  "r12",  "r13",  "r14",  "r15",
		  "xmm0", "xmm1", "xmm2");
	return static_cast<int>(answer);
}

template <>
inline int collatz_length_impl<5ul>(uint64_t *x, uint64_t *){
	uint64_t answer = 0;
	__asm__ volatile (
		"    mov  %1, %%r8  \n"
		"    mov  %2, %%r9  \n"
		"    mov  %3, %%r10 \n"
		"    mov  %4, %%r11 \n"
		"    mov  %5, %%r12 \n"
		"    xor  %0, %0    \n"
		COLLATZ_IMPL_ASSEMBLY_5(collatz5)
		COLLATZ_IMPL_ASSEMBLY_4(collatz5)
		COLLATZ_IMPL_ASSEMBLY_3(collatz5)
		COLLATZ_IMPL_ASSEMBLY_2(collatz5)
		: "=r"(answer)
		: "m"(x[0]), "m"(x[1]), "m"(x[2]), "m"(x[3]),
		  "m"(x[4])
		: "cc",   "rcx",
		  "r8",   "r9",   "r10",  "r11",
		  "r12",  "r13",  "r14",  "r15",
		  "xmm0", "xmm1", "xmm2");
	return static_cast<int>(answer);
}

template <>
inline int collatz_length_impl<4ul>(uint64_t *x, uint64_t *){
	uint64_t answer = 0;
	__asm__ volatile (
		"    mov  %1, %%r8  \n"
		"    mov  %2, %%r9  \n"
		"    mov  %3, %%r10 \n"
		"    mov  %4, %%r11 \n"
		"    xor  %0, %0    \n"
		COLLATZ_IMPL_ASSEMBLY_4(collatz4)
		COLLATZ_IMPL_ASSEMBLY_3(collatz4)
		COLLATZ_IMPL_ASSEMBLY_2(collatz4)
		: "=r"(answer)
		: "m"(x[0]), "m"(x[1]), "m"(x[2]), "m"(x[3])
		: "cc",  "rcx",
		  "r8",  "r9",  "r10", "r11",
		  "r12", "r13", "r14", "r15");
	return static_cast<int>(answer);
}

template <>
inline int collatz_length_impl<3ul>(uint64_t *x, uint64_t *){
	uint64_t answer = 0;
	__asm__ volatile (
		"    mov  %1, %%r8  \n"
		"    mov  %2, %%r9  \n"
		"    mov  %3, %%r10 \n"
		"    xor  %0, %0    \n"
		COLLATZ_IMPL_ASSEMBLY_3(collatz3)
		COLLATZ_IMPL_ASSEMBLY_2(collatz3)
		: "=r"(answer)
		: "m"(x[0]), "m"(x[1]), "m"(x[2])
		: "cc",  "rcx",
		  "r8",  "r9",  "r10", "r11",
		  "r12", "r13", "r14", "r15");
	return static_cast<int>(answer);
}

template <>
inline int collatz_length_impl<2ul>(uint64_t *x, uint64_t *){
	uint64_t answer = 0;
	__asm__ volatile (
		"    mov  %1, %%r8 \n"
		"    mov  %2, %%r9 \n"
		"    xor  %0, %0   \n"
		COLLATZ_IMPL_ASSEMBLY_2(collatz2)
		: "=r"(answer)
		: "m"(x[0]), "m"(x[1])
		: "cc",  "rcx",
		  "r8",  "r9",  "r10", "r11",
		  "r12", "r13", "r14", "r15");
	return static_cast<int>(answer);
}

#undef COLLATZ_IMPL_ASSEMBLY_4
#undef COLLATZ_IMPL_ASSEMBLY_3
#undef COLLATZ_IMPL_ASSEMBLY_2

template <unsigned long INT_PRECISION>
class CollatzSolver {

private:
	using MPInt = std::array<uint64_t, INT_PRECISION>;
	std::vector<std::array<MPInt, 10>> m_table;

	void initialize_table(int width){
		m_table.resize(width);
		for(auto &x : m_table){
			for(auto &y : x){
				for(auto &z : y){ z = 0; }
			}
		}
		for(int j = 1; j < 10; ++j){ m_table[0][j][0] = j; }
		for(int i = 1; i < width; ++i){
			mpint_add<INT_PRECISION>(
				m_table[i][1].data(),
				m_table[i - 1][9].data(), m_table[i - 1][1].data());
			for(int j = 2; j < 10; ++j){
				mpint_add<INT_PRECISION>(
				m_table[i][j].data(),
				m_table[i][j - 1].data(), m_table[i][1].data());
			}
		}
	}

	int collatz_length(const MPInt &a) const {
		std::array<uint64_t, INT_PRECISION + 1> x, y;
		for(int i = 0; i < INT_PRECISION; ++i){ x[i] = a[i]; }
		return collatz_length_impl<INT_PRECISION>(x.data(), y.data());
	}

public:
	CollatzSolver(int max_width = 0)
		: m_table(max_width)
	{
		initialize_table(max_width);
	}

	template <typename Iterator>
	int operator()(Iterator begin, Iterator end) const {
		MPInt x = { 0 };
		int digit = 0;
		for(Iterator it = begin; it != end; ++it){
			if(*it){
				mpint_add<INT_PRECISION>(
					x.data(), x.data(), m_table[digit][*it].data());
			}
			++digit;
		}
		return collatz_length(x);
	}

};

//----------------------------------------------------------------------------
// Solver runner
//----------------------------------------------------------------------------
template <class SubSolver>
inline std::string run_solver(const Problem &problem){
	using Solution = typename SubSolver::Solution;
	using pii = std::pair<int, int>;
	const auto TIME_MARGIN = std::chrono::milliseconds(500);
	const int TRIAL_PER_LOOP = 1000;

	SubSolver solver(problem);
	const auto subtasks = solver.create_subtasks(problem);
	const int n = subtasks.size();
	if(n == 0){ return ""; }
	std::vector<Solution> solutions(n);
	std::vector<int> best_scores(n, -1);

	const auto time_limit =
		std::chrono::system_clock::now() +
		std::chrono::seconds(problem.timeout()) -
		TIME_MARGIN;

	int best_score = -1, task_id = 0, num_trials = 0;
#pragma omp parallel
	{
		std::random_device seed_generator;
		std::default_random_engine random(seed_generator());
		while(task_id >= 0){
			auto result = solver.search(
				subtasks[task_id], best_score, TRIAL_PER_LOOP, random);
			if(result.first > best_score){
#pragma omp critical
				if(result.first > best_score){
					best_score = result.first;
					solutions[task_id] = std::move(result.second);
				}
			}
#pragma omp barrier
#pragma omp single
			{
				++num_trials;
				best_scores[task_id] = best_score;
				if(std::chrono::system_clock::now() >= time_limit){
					task_id = -1;
				}else{
					for(int i = 0; i < n; ++i){
						if(best_scores[i] < best_score){
							best_score = best_scores[i];
							task_id = i;
						}
					}
				}
			}
#pragma omp barrier
		}
	}
#if VERBOSE
	int score_sum = accumulate(best_scores.begin(), best_scores.end(), 0);
	std::cerr << "score      : " << score_sum << std::endl;
	std::cerr << "# of trials: " << num_trials << std::endl;
#endif
	return solver.to_moves(problem, solutions);
}

//----------------------------------------------------------------------------
// Large solver (in-row reordering + horizontal flip)
//----------------------------------------------------------------------------
template <unsigned long INT_PRECISION>
class LargeSolver {

public:
	struct SubTask {
		std::array<int, 2> paddings;
		std::vector<std::array<int, 3>> blocks;
		SubTask()
			: paddings{ -1, -1 }
			, blocks()
		{ }
	};
	using Solution = std::vector<int>;

private:
	CollatzSolver<INT_PRECISION> collatz;

	template <class Iterator, class Random>
	void random_flip(Iterator begin, Iterator end, Random &random){
		for(Iterator it = begin; it != end; ){
			uint32_t r = random() | (1u << 31);
			for(; it != end && r > 1u; ++it, r >>= 1){
				*it ^= -(r & 1);
			}
		}
	}

public:
	LargeSolver() : collatz() { }
	LargeSolver(const Problem &problem) : collatz(problem.width()) { }

	std::vector<SubTask> create_subtasks(const Problem &problem){
		const int width = problem.width(), height = problem.height();
		const int pad_width = width % 3;
		const auto &all_blocks = problem.blocks();
		const int n = all_blocks.size();
		std::vector<int> paddings[2];
		for(int i = 0; i < n; ++i){
			const int k = i % width;
			if(k >= pad_width){ continue; }
			for(const auto x : all_blocks[i]){ paddings[k].push_back(x); }
		}
		const int m = (n * 3) / width;
		std::vector<SubTask> tasks(m);
		for(int i = 0, p = 0; i < m; ++i){
			if(i % 3 == 0){ p += pad_width; }
			for(int j = 0; j < pad_width; ++j){
				tasks[i].paddings[j] = paddings[j][i];
			}
			const int k = width / 3;
			for(int j = 0; j < k; ++j, ++p){
				tasks[i].blocks.push_back(all_blocks[p]);
			}
		}
		return tasks;
	}

	template <class Random>
	std::pair<int, Solution> search(
		const SubTask &task, int best_score, int num_iterations,
		Random &random)
	{
		const int n = task.blocks.size();
		const int m =
			std::find(task.paddings.begin(), task.paddings.end(), -1) -
			task.paddings.begin();
		std::vector<int> order(n), digits(m + 3 * n);
		for(int i = 0; i < n; ++i){ order[i] = i; }
		for(int i = 0; i < m; ++i){ digits[i] = task.paddings[i]; }
		Solution best_solution(n);
		for(int i = 0; i < num_iterations; ++i){
			std::shuffle(order.begin(), order.end(), random);
			random_flip(order.begin(), order.end(), random);
			for(int i = 0; i < n; ++i){
				if(order[i] >= 0){
					const int p = order[i];
					digits[m + i * 3 + 0] = task.blocks[p][0];
					digits[m + i * 3 + 1] = task.blocks[p][1];
					digits[m + i * 3 + 2] = task.blocks[p][2];
				}else{
					const int p = ~order[i];
					digits[m + i * 3 + 0] = task.blocks[p][2];
					digits[m + i * 3 + 1] = task.blocks[p][1];
					digits[m + i * 3 + 2] = task.blocks[p][0];
				}
			}
			const int length = collatz(digits.begin(), digits.end());
			if(length <= best_score){ continue; }
			best_score = length;
			for(int i = 0; i < n; ++i){
				const int x = order[i], mask = (x >> 31);
				best_solution[x ^ mask] = i ^ mask;
			}
		}
		return std::make_pair(best_score, best_solution);
	}

	std::string to_moves(
		const Problem &problem, const std::vector<Solution> &solutions)
	{
		const int n = problem.blocks().size();
		const int width = problem.width(), height = problem.height();
		const int pad_width = width % 3;
		std::ostringstream oss;
		for(int i = 0, j = -1; i < n; ++i){
			const int k = i % width;
			if(k < pad_width){
				const int r = (width - 1) / 2 - k, d = height - 2;
				oss << "DC";
				for(int x = 0; x < r; ++x){ oss << 'R'; }
				for(int x = 0; x < d; ++x){ oss << 'D'; }
			}else{
				const int b = (k - pad_width) % (width / 3);
				if(b == 0){ ++j; }
				if(j >= solutions.size() || solutions[j].empty()){ break; }
				const int s = solutions[j][b];
				const int p = (s >> 31) ^ s;
				const int r = ((width - 1) / 2) - pad_width - 1 - p * 3;
				oss << 'D';
				if(s != p){ oss << "CC"; }
				if(r < 0){
					for(int x = 0; x > r; --x){ oss << 'L'; }
				}else{
					for(int x = 0; x < r; ++x){ oss << 'R'; }
				}
				for(int x = 1; x < height; ++x){ oss << 'D'; }
			}
		}
		return oss.str();
	}

};

//----------------------------------------------------------------------------
// main
//----------------------------------------------------------------------------
int main(int argc, char *argv[]){
	std::vector<std::string> args(argv, argv + argc);
	if(args.size() < 3){
		std::cerr << "Usage: " << args[0] << " seed out-file" << std::endl;
		return 0;
	}
	const uint32_t seed = std::stoul(args[1]);
	const Problem problem(seed);
#ifdef VERBOSE
	std::cerr << "width:       " << problem.width() << std::endl;
	std::cerr << "height:      " << problem.height() << std::endl;
	std::cerr << "num_blocks:  " << problem.numbers() << std::endl;
	std::cerr << "timeout:     " << problem.timeout() << std::endl;
	std::cerr << "num_threads: " << omp_get_max_threads() << std::endl;
#endif
	std::string moves;
	if(problem.width() >= 193){
		moves = run_solver<LargeSolver<12>>(problem);
	}else if(problem.width() >= 174){
		moves = run_solver<LargeSolver<11>>(problem);
	}else if(problem.width() >= 155){
		moves = run_solver<LargeSolver<10>>(problem);
	}else if(problem.width() >= 135){
		moves = run_solver<LargeSolver<9>>(problem);
	}else if(problem.width() >= 116){
		moves = run_solver<LargeSolver<8>>(problem);
	}else if(problem.width() >= 97){
		moves = run_solver<LargeSolver<7>>(problem);
	}else if(problem.width() >= 78){
		moves = run_solver<LargeSolver<6>>(problem);
	}else if(problem.width() >= 58){
		moves = run_solver<LargeSolver<5>>(problem);
	}else if(problem.width() >= 39){
		moves = run_solver<LargeSolver<4>>(problem);
	}else{
		moves = run_solver<LargeSolver<3>>(problem);
	}
	std::ofstream ofs(args[2].c_str());
	ofs << moves << std::endl;
	ofs.close();
	return 0;
}

