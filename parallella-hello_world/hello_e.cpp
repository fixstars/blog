#include <stdio.h>

#include "e_lib.h"

int main(void)
{
    e_coreid_t core_id = e_get_coreid();
    char* out_buff = (char*)(0x2000);
    int* finish_flag = (int*)(0x7000);

    sprintf(out_buff, "Hello world from core %d\n", core_id);

    *finish_flag = 0x1;

    return 0;
}
