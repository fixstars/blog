#include <iostream>
#include <vector>
#include <string>

#include <e-hal.h>

#define _BUFFER_SIZE (128)

int main()
{
    e_platform_t platform;
    e_epiphany_t dev;

    e_init(NULL);
    e_reset_system();
    e_get_platform_info(&platform);

    // open all cores
    e_open(&dev, 0, 0, platform.rows, platform.cols);
    e_reset_group(&dev);

    std::vector<int> finish_flags(platform.rows * platform.cols);
    std::vector<std::string> messages(platform.rows * platform.cols);

    // load file
    e_load_group((char*)"hello_e.srec", &dev, 0, 0, platform.rows, platform.cols, E_TRUE);

    // check all cores are done
    while(1) {
        int all_done = 1;
        for(size_t i = 0; i < platform.rows; ++i) {
            for(size_t j = 0; j < platform.cols; ++j) {
                e_read(&dev, i, j, 0x7000, &finish_flags[i * platform.cols + j], sizeof(int));
                all_done &= (finish_flags[i * platform.cols + j]) & 0x1;
            }
        }

        if(all_done) {
            break;
        }
    }

    // read message
    for(size_t i = 0; i < platform.rows; ++i) {
        for(size_t j = 0; j < platform.cols; ++j) {
            char tmp_buffer[_BUFFER_SIZE];
            e_read(&dev, i, j, 0x2000, tmp_buffer, sizeof(char) * _BUFFER_SIZE);
            messages[i * platform.cols + j] = tmp_buffer;
        }
    }
    // print message
    for(size_t i = 0; i < platform.rows; ++i) {
        for(size_t j = 0; j < platform.cols; ++j) {
            std::cout << "(" << i << "," << j << ") " << messages[i * platform.cols + j];
        }
    }

    e_close(&dev);
    e_finalize();


    return 0;
}
