#!/bin/bash

set -e

ESDK=${EPIPHANY_HOME}
ELIBS=${ESDK}/tools/host/lib
EINCS=${ESDK}/tools/host/include
ELDF=${ESDK}/bsps/current/fast.ldf

# Build HOST side application
g++ hello_host.cpp -o hello_world.elf -I ${EINCS} -L ${ELIBS} -le-hal

# Build DEVICE side program
e-g++ -T ${ELDF} hello_e.cpp -o hello_e.elf -le-lib

# Convert ebinary to SREC file
e-objcopy --srec-forceS3 --output-target srec hello_e.elf hello_e.srec

