#define __CL_ENABLE_EXCEPTIONS

#include <stdio.h>
#include <windows.h>
#include <CL/cl.hpp>
#include <CL/cl_ext.h>
#include <iostream>
#include <immintrin.h>
#include <assert.h>
#include <atomic>

#define LIST_DECL(G)                            \
    struct List {                               \
        int value;                              \
        G struct List *chain;                   \
    };

LIST_DECL();

#define S_(a) #a
#define S(a) S_(a)

#define KERNEL ""                                       \
    S(LIST_DECL(__global))                                              \
    S(__kernel void f(__global int *sum_ret, __global struct List *p) { \
            int sum = 0;                                                \
            while (p) {                                 \
                sum += p->value;                        \
                p = p->chain;                           \
            }                                           \
            *sum_ret = sum;                             \
        }                                               \
        )                                               \

cl::Platform cl_plt;
cl::Device cl_device;
cl::Context gpu_context;
cl::CommandQueue gpu_queue;
cl::Kernel kernel;
std::vector<void*> svm_pointers;

struct List2
    :public List
{
    void *operator new (size_t count) {
        cl_svm_mem_flags alloc_flags = CL_MEM_READ_WRITE|CL_MEM_SVM_ATOMICS|CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS;
        void *ptr = clSVMAlloc(gpu_context(),
                               alloc_flags,
                               count * sizeof(List2),
                               0);

        svm_pointers.push_back(ptr);

        return ptr;
    }

    void operator delete (void *ptr) {
        clSVMFree(gpu_context(), ptr);
    }
};


int
setup_opencl(void)
{
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);

    if (platforms.size() == 0) {
        puts("cl platform not found");
        return -1;
    }

    bool found = false;

    for (int pid=0; pid<(int)platforms.size(); pid++) {
        std::string name;
        platforms[pid].getInfo(CL_PLATFORM_VENDOR, &name);

        if (strncmp(name.c_str(), "Intel", 5) == 0) {
            std::vector<cl::Device> gpus;

            try {
                platforms[pid].getDevices(CL_DEVICE_TYPE_GPU, &gpus);
            } catch (cl::Error err) {
                gpus.clear();
            }

            for (int di=0; di<(int)gpus.size(); di++) {
                cl_device_svm_capabilities svm_capability;
                std::string dev_name;
                gpus[di].getInfo(CL_DEVICE_NAME, &dev_name);
                gpus[di].getInfo(CL_DEVICE_SVM_CAPABILITIES, &svm_capability);

                printf("svm capability(%s) : %s%s%s%s\n",
                       dev_name.c_str(),
                       (svm_capability&CL_DEVICE_SVM_COARSE_GRAIN_BUFFER?"coarse grain, ":""),
                       (svm_capability&CL_DEVICE_SVM_FINE_GRAIN_BUFFER?"fine grain buffer, ":""),
                       (svm_capability&CL_DEVICE_SVM_FINE_GRAIN_SYSTEM?"fine grain system, ":""),
                       (svm_capability&CL_DEVICE_SVM_ATOMICS?"atomic, ":"")
                    );

                if ((svm_capability & CL_DEVICE_SVM_FINE_GRAIN_BUFFER) &&
                    (svm_capability & CL_DEVICE_SVM_ATOMICS))
                {
                    found = true;

                    cl_plt = platforms[pid];
                    cl_device = gpus[di];

                    std::string dev_name;
                    gpus[di].getInfo(CL_DEVICE_NAME, &dev_name);
                    printf("%s:%s\n", name.c_str(), dev_name.c_str());
                }

            }
        }
    }

    if (!found) {
        puts("fine grain buffer svm capable gpu is not found");
        return -1;
    }

    gpu_context = cl::Context(cl_device);
    gpu_queue = cl::CommandQueue(gpu_context);

    std::vector<cl::Device> gpu_devs;
    gpu_devs.push_back(cl_device);

    cl::Program::Sources source(1,
                                std::make_pair(KERNEL, strlen(KERNEL)));
    cl::Program prog = cl::Program(gpu_context, source);
    try {
        prog.build(gpu_devs, "-cl-std=CL2.0");
    } catch (...) {
        std::string log;
        log = prog.getBuildInfo<CL_PROGRAM_BUILD_LOG>(gpu_devs[0]);
        puts(log.c_str());
        throw;
    }

    cl_int err;
    kernel = cl::Kernel(prog, "f", &err);

    return 0;
}

static double
delta_usec(LARGE_INTEGER start,
           LARGE_INTEGER end,
           LARGE_INTEGER freq)
{
    LONGLONG delta = end.QuadPart - start.QuadPart;

    return (delta * 1000000.0) / freq.QuadPart;
}

static void
svm_perf(void)
{
    LARGE_INTEGER freq, start, end;;
    cl_svm_mem_flags alloc_flags = CL_MEM_READ_WRITE|CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS;

    QueryPerformanceFrequency(&freq);

    void *svm_ptr;

    QueryPerformanceCounter(&start);
    svm_ptr = clSVMAlloc(gpu_context(), alloc_flags, 1, 64);
    QueryPerformanceCounter(&end);

    double alloc = delta_usec(start, end, freq);

    QueryPerformanceCounter(&start);
    clSVMFree(gpu_context(), svm_ptr);
    QueryPerformanceCounter(&end);

    double free = delta_usec(start, end, freq);

    printf("alloc        1byte : %f[usec]\n", alloc);
    printf("free         1byte : %f[usec]\n", free);
}

static void
run(void)
{
    List2 *l = NULL;
    int *ret_val = (int*)clSVMAlloc(gpu_context(), CL_MEM_WRITE_ONLY, sizeof(int), 0);
    for (int i=1; i<=10; i++) {
        List2 *l2 = new List2();
        l2->value = i;
        l2->chain = l;

        l = l2;
    }

    clSetKernelExecInfo(kernel(),
                        CL_KERNEL_EXEC_INFO_SVM_PTRS,
                        svm_pointers.size() * sizeof(void*),
                        svm_pointers.data());

    cl_int r;
    r = clSetKernelArgSVMPointer(kernel(), 0, ret_val);
    r = clSetKernelArgSVMPointer(kernel(), 1, l);

    gpu_queue.enqueueTask(kernel);
    gpu_queue.finish();

    int sum_host = 0;
    List *l_host = l;
    while (l_host) {
        sum_host += l_host->value;
        l_host = l_host->chain;
    }

    //printf("%d %d\n", *ret_val, sum_host);
    assert(*ret_val == sum_host);

    clSVMFree(gpu_context(), ret_val);

    while (l) {
        List2 *l2 = (List2*)l->chain;
        delete l2;
        l = l2;
    }
}

static int
main2(int argc, char **argv)
{
    int r;
    r = setup_opencl();

    svm_perf();

    run();

    return r;
}


int
main(int argc, char **argv)
{
    try {
        main2(argc, argv);
    } catch (cl::Error err) {
        std::cerr
            << "ERROR: "
            << err.what()
            << "("
            << err.err()
            << ")"
            << std::endl;
    }
}


