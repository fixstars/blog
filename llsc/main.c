#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#ifdef __x86_64__
#include <immintrin.h>
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdatomic.h>

#ifdef __arm__
#define ldrex(ret,ptr) __asm__ __volatile__ ("ldrex %0, [%1]\n\t":"=r"(ret):"r"(ptr))
#define strex(cond,val,ptr) __asm__ __volatile__ ("strex %0, %1, [%2]\n\t":"=&r"(cond):"r"(val),"r"(ptr))
#endif

static int
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags )
{
    int ret;

    ret = syscall( __NR_perf_event_open, hw_event, pid, cpu,
                   group_fd, flags );
    return ret;
}

char buffer[4096] __attribute__((aligned(4096)));

int main()
{
    struct perf_event_attr attr;
    int perf_fd;
    long long val0, val1;
    long long tsc0, tsc1;
    int i, j;
    int nloop = 1024*1024;

    memset(&attr, 0, sizeof(attr));

    attr.type = PERF_TYPE_HARDWARE;
    attr.size = sizeof(attr);
    attr.config = PERF_COUNT_HW_CPU_CYCLES;

    perf_fd = perf_event_open(&attr, 0, -1, -1, 0);
    if (perf_fd == -1) {
        perror("perf_event_open");
        exit(1);
    }

#ifdef __RTM__
    for (i=0; i<4; i++) {
        memset(buffer, 0, sizeof(buffer));

        int fail_count = 0;
        read(perf_fd, &val0, sizeof(val0));
        for (j=0; j<nloop; j++) {
            int r = _xbegin();
            if (r == _XBEGIN_STARTED) {
                buffer[64*0] = 1;
                _xend();
            } else {
                fail_count++;
            }
        }
        read(perf_fd, &val1, sizeof(val0));
        printf("xbegin+store+xend : %f [clk], fail=%d\n", (val1-val0)/(double)nloop, fail_count);
    }

    for (i=0; i<4; i++) {
        memset(buffer, 0, sizeof(buffer));

        int fail_count = 0;
        read(perf_fd, &val0, sizeof(val0));
        for (j=0; j<nloop; j++) {
            int r = _xbegin();
            if (r == _XBEGIN_STARTED) {
                buffer[64*0] = 1;
                buffer[64*1] = 1;
                buffer[64*2] = 1;
                buffer[64*3] = 1;
                buffer[64*4] = 1;
                buffer[64*5] = 1;
                buffer[64*6] = 1;
                buffer[64*7] = 1;
                _xend();
            } else {
                fail_count++;
            }
        }
        read(perf_fd, &val1, sizeof(val0));
        printf("xbegin+storex8+xend : %f [clk], fail=%d\n", (val1-val0)/(double)nloop, fail_count);
    }
#endif


#ifdef __x86_64__
    for (i=0; i<4; i++) {
        memset(buffer, 0, sizeof(buffer));

        int fail_count = 0;
        int val = 1;
        int prev = 1;

        read(perf_fd, &val0, sizeof(val0));
        for (j=0; j<nloop; j++) {
            atomic_compare_exchange_strong(&val, &prev, 1);
        }
        read(perf_fd, &val1, sizeof(val0));
        printf("cas : %f [clk], fail=%d\n", (val1-val0)/(double)nloop, fail_count);
    }
#endif

#ifdef __arm__
    for (i=0; i<4; i++) {
        memset(buffer, 0, sizeof(buffer));

        int fail_count = 0;
        int val = 1;
        int prev = 1;

        read(perf_fd, &val0, sizeof(val0));
        for (j=0; j<nloop; j++) {
            int v, cond;
            ldrex(v, &val);
            v++;
            strex(cond, v, &val);
        }
        read(perf_fd, &val1, sizeof(val0));
        printf("ldrex+strex : %f [clk], fail=%d\n", (val1-val0)/(double)nloop, fail_count);
    }
#endif

    for (i=0; i<4; i++) {
        memset(buffer, 0, sizeof(buffer));

        int fail_count = 0;
        int val = 0;
        int next = 1;
        int prev = 0;

        read(perf_fd, &val0, sizeof(val0));
        for (j=0; j<nloop; j++) {
            (*(volatile int*)&val)++;
        }
        read(perf_fd, &val1, sizeof(val0));
        printf("store : %f [clk], fail=%d\n", (val1-val0)/(double)nloop, fail_count);
    }

    close(perf_fd);
}
