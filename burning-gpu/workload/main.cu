#include <cstdio>
#include <chrono>
#include <time.h>
#include <cublas_v2.h>
#include <thrust/device_vector.h>

inline double get_current_epoch(){
	timespec tp;
	clock_gettime(CLOCK_REALTIME, &tp);
	return tp.tv_sec + tp.tv_nsec * 1e-9;
}

int main(int argc, char *argv[]){
	namespace chrono = std::chrono;
	using duration_type = chrono::duration<double>;
	const int n = 8192;

	if(argc < 2){
		printf("Usage: %s device-id\n", argv[0]);
		return 0;
	}
	cudaSetDevice(atoi(argv[1]));

	cublasHandle_t handle;
	cublasCreate(&handle);

	const float alpha = 1.0f, beta = 0.0f;
	thrust::device_vector<float> A(n * n), B(n * n), C(n * n);
	while(true){
		const auto start = chrono::steady_clock::now();
		cublasSgemm(
			handle, CUBLAS_OP_N, CUBLAS_OP_N,
			n, n, n,
			&alpha,
			A.data().get(), n,
			B.data().get(), n,
			&beta,
			C.data().get(), n);
		cudaDeviceSynchronize();
		const auto stop = chrono::steady_clock::now();
		const auto duration = chrono::duration_cast<duration_type>(stop - start);
		const auto flops = (2.0 * n * n * n) / duration.count();
		const auto timestamp = get_current_epoch();
		printf("%.9f\t%.3f\n", timestamp, flops * 1e-9);
	}

	cublasDestroy(handle);
	return 0;
}

