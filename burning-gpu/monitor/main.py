# -*- coding: utf-8 -*-
import sys
import time
from datetime import datetime
from py3nvml.py3nvml import *

class Device:

    def __init__(self, device_id):
        self.__device = nvmlDeviceGetHandleByIndex(device_id)

    def temperature(self):
        return nvmlDeviceGetTemperature(self.__device, NVML_TEMPERATURE_GPU)

    def graphics_clock(self):
        return nvmlDeviceGetClockInfo(self.__device, NVML_CLOCK_GRAPHICS)

    def sm_clock(self):
        return nvmlDeviceGetClockInfo(self.__device, NVML_CLOCK_SM)

    def memory_clock(self):
        return nvmlDeviceGetClockInfo(self.__device, NVML_CLOCK_MEM)

    def fan_speed(self):
        return nvmlDeviceGetFanSpeed(self.__device)

    def power_usage(self):
        return nvmlDeviceGetPowerUsage(self.__device) * 1e-3


def main():
    nvmlInit()
    device = Device(int(sys.argv[1]))
    while True:
        print('{:.9f},{},{},{},{},{:.3f}'.format(
            datetime.now().timestamp(),
            device.temperature(),
            device.sm_clock(),
            device.memory_clock(),
            device.fan_speed(),
            device.power_usage()))
        time.sleep(0.01)
    nvmlShutdown()

if __name__ == '__main__':
    main()
