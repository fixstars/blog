import argparse
import skimage
import skimage.io
import skimage.transform
import numpy as np
import cntk as C

parser = argparse.ArgumentParser(description='Inference ONNX model with CNTK')
parser.add_argument('--model', '-m', required=True, type=str)
parser.add_argument('--input', '-i', type=str)
parser.add_argument('--synset', '-s', default='synset_words.txt')
parser.add_argument('--topN', default=5, type=int)
args = parser.parse_args()

# load input image
print('loading image ...');
if args.input:
    img = skimage.img_as_float(skimage.io.imread(args.input))
    img *= 255  # [0 ... 1] -> [0 ... 255]
    img = skimage.transform.resize(img, (224, 224)).astype(np.float32)
    img = img[..., [2, 1, 0]]  # RGB -> BGR
    img = np.rollaxis(img, 2)  # HWC -> CHW
else:
    # generate zero image if input is not given
    img = np.zeros((3, 224, 224), dtype=np.float32)

# load the ONNX model
print('loading ONNX model ...')
model = C.Function.load(args.model, device=C.device.cpu(), format=C.ModelFormat.ONNX)

# run inference with CNTK
print('running inference ...')
prediction = np.squeeze(model.eval({model.arguments[0]:[img]}))
ranking = np.argsort(prediction)[::-1]

with open(args.synset) as f:
    synset = f.readlines()
    for i in range(args.topN):
        print('{} {}'.format(synset[ranking[i]].strip(), prediction[ranking[i]]))
