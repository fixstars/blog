import argparse
import numpy as np
import chainer
import chainer.links as L
import onnx_chainer
import onnx

parser = argparse.ArgumentParser(description='Convert Chainer model to ONNX')
parser.add_argument('--output', '-o', required=True, type=str)
args = parser.parse_args()

# obtain PyTorch model
model = L.ResNet152Layers()

# export to ONNF
print('converting to ONNX')
dummy_input = np.zeros((1, 3, 224, 224), dtype=np.float32)
chainer.config.train = False
onnx_chainer.export(model, dummy_input, filename=args.output)

# Check output model
print('checking converted model')
onnx_model = onnx.load(args.output)
onnx.checker.check_model(onnx_model)
