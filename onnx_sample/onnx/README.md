# ONNX サンプルスクリプト

## ONNX Export
下記のフレームワークから ONNX モデルを出力します。
* Caffe2 (`caffe2onnx.py`)
* Chainer (`chainer2onnx.py`)
* CNTK (`cntk2onnx.py`)
* PyTorch (`pytorch2onnx.py`)

### 使い方
各プログラムごとに、必要とするパラメータが異なります。
全プログラムに共通するパラメータは次のひとつです。
* `-o / --output`: 出力する ONNX モデルのファイル名を指定します

`caffe2onnx.py` は以下のパラメータを持ちます。
* `--predict`: 変換対象モデルの `predict.pb` のパスを指定します
* `--init`: 変換対象モデルの `init.pb` のパスを指定します

`cntk2onnx.py` は以下のパラメータを持ちます。
* `-i / --input`: 変換対象モデルのパスを指定します

PyTorch と Chainer に関しては、それぞれのフレームワークで既に定義されている訓練済みモデルを出力するため、任意のモデルを変換することはできません。

## ONNX Import
下記のフレームワークを用いて ONNX モデルをインポートし、ImageNet の 1000 クラス分類をします。
* Caffe2 (`inference_caffe2.py`)
* CNTK (`inference_cntk.py`)
* MXNet (`inference_mxnet.py`)
* TensorFlow (`inference_tensorflow.py`)

### 使い方
すべてのスクリプトで、共通のパラメータを持ちます。
* `-m / --model`: 使用する ONNX モデルを指定します
* `-i / --input`: 入力画像を指定します。省略された場合は、画素値がすべて0の画像が入力として扱われます。

使用例
```
# python3 inference_caffe.py -i in/jo7ueb_logo.jpg -m zoo/vgg19/model.onnx
```