import argparse
import onnx
import caffe2.python.onnx.frontend as c2fe
from caffe2.proto import caffe2_pb2

parser = argparse.ArgumentParser(description='Convert Caffe2 model to ONNX')
parser.add_argument('--predict', required=True, type=argparse.FileType('rb'))
parser.add_argument('--init',    required=True, type=argparse.FileType('rb'))
parser.add_argument('--output', '-o', required=True, type=argparse.FileType('wb'))
args = parser.parse_args()

# load Caffe2 model
predict_net = caffe2_pb2.NetDef()
print('loading predict_net.pb');
predict_net.ParseFromString(args.predict.read())

init_net = caffe2_pb2.NetDef()
print('loading init_net.pb')
init_net.ParseFromString(args.init.read())

# export to ONNF
data_type = onnx.TensorProto.FLOAT
data_shape = (1, 3, 224, 224)
value_info = {'data': (data_type, data_shape)}

print('converting to ONNX')
onnx_model = c2fe.caffe2_net_to_onnx_model(predict_net, init_net, value_info)
print('checking converted model')
onnx.checker.check_model(onnx_model)

print('saving ONNX model')
args.output.write(onnx_model.SerializeToString())
