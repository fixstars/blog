import argparse
from torch.autograd import Variable
import torch.onnx
import torchvision.models as models
import onnx

parser = argparse.ArgumentParser(description='Convert PyTorch model to ONNX')
parser.add_argument('--output', '-o', required=True, type=str)
args = parser.parse_args()

# obtain PyTorch model
model = models.vgg16_bn(pretrained=True)

# export to ONNF
dummy_input = Variable(torch.randn(1, 3, 224, 224))

print('converting to ONNX')
torch.onnx.export(model, dummy_input, args.output)

print('checking converted model')
onnx_model = onnx.load(args.output)
onnx.checker.check_model(onnx_model)
