import argparse
import cntk as C
import onnx

parser = argparse.ArgumentParser(description='Convert CNTK model to ONNX')
parser.add_argument('--input',  '-i', required=True, type=str)
parser.add_argument('--output', '-o', required=True, type=str)
args = parser.parse_args()

# obtain PyTorch model
model = C.Function.load(args.input, device=C.device.cpu())

# export to ONNF
print('converting to ONNX')
model.save(args.output, format=C.ModelFormat.ONNX)

print('checking converted model')
onnx_model = onnx.load(args.output)
onnx.checker.check_model(onnx_model)
