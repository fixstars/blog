#include "Particle.hpp"
#include <numeric>

namespace OpenMps
{
	Particle::Particle(const double& x, const double& z, const double& u, const double& w, const double& p, const double& n)
	{
		// 各物理値を初期化
		this->x[0] = x;
		this->x[1] = z;
		this->u[0] = u;
		this->u[1] = w;
		this->p = p;
		this->n = n;
	}

	void Particle::UpdateNeighborDensity(const Particle::List& particles, const Grid& grid, const double& r_e)
	{
		// 重み関数の総和を粒子数密度とする
		// TODO: 全粒子探索してるので遅い
		n = std::accumulate(grid.cbegin(this->x), grid.cend(this->x), 0.0,
			[this, &r_e, &particles, &grid](const double& sum, const Grid::BlockID block)
			{
				auto neighbors = grid[block];

				return sum + std::accumulate(neighbors.cbegin(), neighbors.cend(), 0.0,
					[this, &r_e, &particles](const double& sum2, const int& id)
					{
						double w = this->Weight(*(particles[id]), r_e);
						return sum2 + w;
					});
			});
	}
}