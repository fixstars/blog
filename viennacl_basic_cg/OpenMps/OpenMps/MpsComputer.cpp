#include "MpsComputer.hpp"
#include <algorithm>
#include <cmath>
#ifdef USE_VIENNACL
#include <viennacl/linalg/prod.hpp>
#include <viennacl/linalg/inner_prod.hpp>
#include <viennacl/linalg/norm_inf.hpp>
#endif
#ifdef _OPENMP
#include <omp.h>
#endif

namespace OpenMps
{
	MpsComputer::MpsComputer(
			const double& g,
			const double& rho,
			const double& nu,
			const double& C,
			const double& r_eByl_0,
			const double& surfaceRatio,
			const double& allowableResidual,
			const double& l_0,
			const Particle::List& particles)
		: g(), rho(rho), r_e(r_eByl_0 * l_0), surfaceRatio(surfaceRatio),

		// 最大時間刻みは、dx < 1/2 g dt^2 （重力による等加速度運動での時間刻み制限）と、指定された引数のうち小さい方
		dt(std::sqrt(2*C*l_0/g)),

		particles(particles),
		grid(r_eByl_0 * l_0)
	{
		// 重力加速度を設定
		this->g[0] = 0;
		this->g[1] = -g;

		// 圧力方程式の許容誤差を設定
		ppe.allowableResidual = allowableResidual;

		// 基準粒子数密度とλの計算
		int range = (int)std::ceil(r_eByl_0);
		n0 = 0;
		lambda = 0;
		for(int i = -range; i < range; i++)
		{
			for(int j = -range; j < range; j++)
			{
				// 自分以外との
				if(!((i == 0) && (j == 0)))
				{
					// 相対位置を計算
					Vector x;
					x[0] = i*l_0;
					x[1] = j*l_0;

					// 重み関数を計算	
					double r = boost::numeric::ublas::norm_2(x);
					auto w = Particle::Weight(r, r_e);

					// 基準粒子数密度に足す
					n0 += w;

					// λに足す
					lambda += r*r * w;
				}
			}
		}

		// λの最終計算
		// λ = (Σr^2 w)/(Σw)
		lambda /= n0;

		// 全粒子について
		for(int i = 0; i < (int)particles.size(); i++)
		{
			// グリッドに登録
			grid.AddParticle(particles[i]->VectorX(), i);
		}

		// 粒子数密度を計算する
		std::cout << "Updating Particle Density..." << std::endl;
#ifdef _OPENMP
		#pragma omp parallel for
#endif
		for(int i = 0; i < (int)particles.size(); i++)
		{
			// 粒子数密度を計算する
			particles[i]->UpdateNeighborDensity(particles, grid, r_e);
		}
		
		std::cout << "Constructing P.P.E...." << std::endl;

		
		// 粒子数を取得
		int n = (int)particles.size();

		{
			// 係数行列を作成
			ppe.A = MatrixPtr(new Matrix(n, n));
			ppe.x = LongVectorPtr(new LongVector(n));
			ppe.b = LongVectorPtr(new LongVector(n));
			ppe.cg.r = LongVectorPtr(new LongVector(n));
			ppe.cg.p = LongVectorPtr(new LongVector(n));
			ppe.cg.Ap = LongVectorPtr(new LongVector(n));	
#ifdef USE_VIENNACL
			ppe.tempA = TempMatrix(n, n);
			ppe.tempB = TempLongVector(n);
			ppe.tempX = TempLongVector(n, n);
#endif

		}

		// 行列成分
		typedef std::pair<int, double> A_ij;
		std::vector< std::vector<A_ij> > a;
		a.resize(n);

		// 係数行列初期化
#ifdef USE_VIENNACL
		auto& A = ppe.tempA;
		auto& x = ppe.tempX;
		auto& b = ppe.tempB;
#else
		auto& A = *(ppe.A);
		auto& x = *(ppe.x);
		auto& b = *(ppe.b);
#endif
		A.clear();

#ifdef _OPENMP		
		#pragma omp parallel
#endif
		{
#ifdef _OPENMP
			#pragma omp for
#endif
			// 全粒子で
			for(int i = 0; i < n; i++)
			{
				// 生成項を計算する
				double b_i = particles[i]->Source(n0, dt, surfaceRatio);
				b(i) = b_i;

				// 未知数ベクトルを0に初期化する
				double x_i = 0;
				x(i) = x_i;
			}
			// TODO: 以下もそうだけど、圧力方程式を作る際にインデックス指定のfor回さなきゃいけないのが気持ち悪いので、どうにかしたい
			
#ifdef _OPENMP
			#pragma omp for
#endif
			// 全粒子で
			for(int i = 0; i < n; i++)
			{
				// 対角項を初期化
				double a_ii = 0;

				// 粒子の位置を取得
				auto x = particles[i]->VectorX();

				// 近傍ブロックに対して
				for(auto block = grid.cbegin(x); block != grid.cend(x); block++)
				{
					auto blockID = *block;

					// その中に含まれている近傍粒子候補に対して
					for(auto j : grid[blockID])
					{
						// 自分以外
						if(i != j)
						{
							// 非対角項を計算
							double a_ij = particles[i]->Matrix(*particles[j], n0, r_e, lambda, rho, surfaceRatio);
							if(a_ij != 0)
							{
								// 非対角項を格納
								a[i].push_back(A_ij(j, a_ij));

								// 対角項も設定
								a_ii -= a_ij;
							}
						}
					}
				}

				// 対角項を格納
				a[i].push_back(A_ij(i, a_ii));
			}
		}

		// 全行の
		for(int i = 0; i < n; i++)
		{
			// 全有効列で
			for(auto k : a[i])
			{
				// 列番号と値を取得
				int j = k.first;
				double a_ij = k.second;

				// 行列に格納
				A(i,j) = a_ij;
			}
		}
	}


	void MpsComputer::Initialize()
	{
#ifdef USE_VIENNACL
		// 作成した方程式をデバイス側に複製
		viennacl::copy(ppe.tempA, *(ppe.A));
		viennacl::fast_copy(ppe.tempX, *(ppe.x));
		viennacl::fast_copy(ppe.tempB, *(ppe.b));

		// 終了待機
		viennacl::backend::finish();
#endif
	}

	void MpsComputer::Solve()
	{
		// 共役勾配法で解く
		// TODO: 前処理ぐらい入れようよ

		auto& A = *(ppe.A);
		auto& x = *(ppe.x);
		auto& b = *(ppe.b);
		auto& r = *(ppe.cg.r);
		auto& p = *(ppe.cg.p);
		auto& Ap = *(ppe.cg.Ap);

		// 使用する演算を選択
#ifdef USE_VIENNACL
		namespace blas = viennacl::linalg;
#else
		namespace blas = boost::numeric::ublas;
#endif

		// 初期値を設定
		//  (Ap)_0 = A * x
		//  r_0 = b - Ap
		//  p_0 = r_0
		//  rr = r・r
		Ap = blas::prod(A, x);
		r = b - Ap;
		p = r;
		double rr = blas::inner_prod(r, r);

		// 初期値でまだ収束していない場合
		bool isConverged = (blas::norm_inf(r)< ppe.allowableResidual);
		if(!isConverged)
		{
			// 未知数分だけ繰り返す
			for(unsigned int i = 0; i < 20000; i++)
			{
				// 計算を実行
				//  Ap = A * p
				//  α = rr/(p・Ap)
				//  x' += αp
				//  r' -= αAp
				Ap = blas::prod(A, p);
				double alpha = rr / blas::inner_prod(p, Ap);
				x += alpha * p;
				r -= alpha * Ap;

				// 収束判定（残差＝残差ベクトルの最大要素値）
				double residual = blas::norm_inf(r);
				isConverged = (residual < ppe.allowableResidual);

#ifdef SHOW_RESIDUAL
				std::cout << "	#" << i << " : " << residual << std::endl;
#endif

				// 収束していたら
				if(isConverged)
				{
					// 繰り返し終了
					break;
				}
				// なかったら
				else
				{
					// 残りの計算を実行
					//  r'r' = r'・r'
					//  β= r'r'/rr
					//  p = r' + βp
					//  rr = r'r'
					double rrNew = blas::inner_prod(r, r);
					double beta = rrNew / rr;
					p = r + beta * p;
					rr = rrNew;
				}
			}
		}

		// 理論上は未知数分だけ繰り返せば収束するはずだが、収束しなかった場合は
		if(!isConverged)
		{
			// どうしようもないので例外
			Exception exception;
			exception.Message = "Conjugate Gradient method couldn't solve Pressure Poison Equation";
//			throw exception;
		}

#ifdef USE_VIENNACL
		// 終了待機
		viennacl::backend::finish();
#endif
	};

	void MpsComputer::Finalize()
	{
#ifdef USE_VIENNACL
		// 計算結果をホスト側に複製
		viennacl::fast_copy(*(ppe.x), ppe.tempX);
		auto& x = ppe.tempX;
#else
		auto& x = *(ppe.x);
#endif

		// 得た圧力を代入する
		for(unsigned int i = 0; i < particles.size(); i++)
		{
			particles[i]->P(x(i), n0, surfaceRatio);
		}

#ifdef USE_VIENNACL
		// 終了待機
		viennacl::backend::finish();
#endif
	}
}
