#ifndef DEFINE_INCLUDED
#define DEFINE_INCLUDED

// 次元数
#define DIMENSTION 2;

// ViennaCLを使用する。
#define USE_VIENNACL

// OpenCLを使用する。
//#define USE_OPENCL

// CUDAを使用する。
//#define USE_CUDA

// 収束状況の表示
//#define SHOW_RESIDUAL

/***********************************************************************/

#ifdef USE_VIENNACL
	#ifdef USE_OPENCL
		// ViennCLでOpenCLを使用する
		#define VIENNACL_WITH_OPENCL
	#else
		#ifdef USE_CUDA
			// ViennCLでCUDAを使用する（※まだ動かない）
			#define VIENNACL_WITH_CUDA
			#error CUDAでのViennaCLは未実装です
		#else
			#ifdef _OPENMP
				// ViennaCLでOpenMPを使用する
				#define VIENNACL_WITH_OPENMP
			#endif
		#endif
	#endif
#else
	#ifdef USE_OPENCL
		// APPMLを使用する（※まだ動かない）
		#define USE_APPML
		#error APPMLは未実装です
	#else
		#ifdef USE_CUDA
			// CUBLAS&CUSPARSEを使用する（※まだ動かない）
			#define USE_CUBLAS
			#error CUBLAS&CUSPARSEは未実装です
		#endif
	#endif
#endif

#endif
