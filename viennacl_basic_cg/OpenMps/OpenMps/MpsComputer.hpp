#ifndef MPSCOMPUTER_INCLUDED
#define MPSCOMPUTER_INCLUDED
#include "defines.hpp"

#include <vector>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#ifdef USE_VIENNACL
#include <viennacl/compressed_matrix.hpp>
#include <viennacl/vector.hpp>
#endif
#ifdef USE_CUBLAS
#include<cublas_v2.h>
#include<cusparse_v2.h>
#endif
#include "Particle.hpp"
#include "Grid.hpp"

namespace OpenMps
{
	// MPS法による計算空間
	class MpsComputer
	{
	private:
#ifdef USE_VIENNACL
		// 線形方程式用の疎行列
		typedef viennacl::compressed_matrix<double> Matrix;

		// 線形方程式用の多次元ベクトル
		typedef viennacl::vector<double> LongVector;


		// CPU用疎行列（方程式の作成に使用）
		typedef boost::numeric::ublas::compressed_matrix<double> TempMatrix;

		// CPU用疎行列（方程式の作成に使用）
		typedef boost::numeric::ublas::vector<double> TempLongVector;
#else
		// 線形方程式用の疎行列
		typedef boost::numeric::ublas::compressed_matrix<double> Matrix;

		// 線形方程式用の多次元ベクトル
		typedef boost::numeric::ublas::vector<double> LongVector;
#endif
		// 線形方程式用の疎行列のポインタ
		typedef std::shared_ptr<Matrix> MatrixPtr;

		// 線形方程式用の多次元ベクトルのポインタ
		typedef std::shared_ptr<LongVector> LongVectorPtr;


		// 近傍粒子探索用グリッド
		Grid grid;

		// 粒子リスト
		Particle::List particles;

		// 時間刻み
		double dt;

		// 基準粒子数密度
		double n0;

		// 重力加速度
		Vector g;

		// 密度
		double rho;

		// 影響半径
		double r_e;

		// 自由表面を判定する係数
		double surfaceRatio;

		// 拡散モデル定数
		double lambda;

		// 圧力方程式
		struct Ppe
		{
			// 係数行列
			MatrixPtr A;

			// 圧力方程式の未知数
			LongVectorPtr x;

			// 圧力方程式の右辺
			LongVectorPtr b;

#ifdef USE_VIENNACL
			// CPU用疎行列（方程式の作成に使用）
			TempMatrix tempA;

			// CPU用未知数（方程式の作成に使用）
			TempLongVector tempX;
			
			// CPU用右辺（方程式の作成に使用）
			TempLongVector tempB;
#endif
			
			// 収束判定（許容誤差）
			double allowableResidual;
			
			// 共役勾配法で使う用
			struct ConjugateGradient
			{
				// 残差ベクトル
				LongVectorPtr r;

				// 探索方向ベクトル
				LongVectorPtr p;

				// 係数行列と探索方向ベクトルの積
				LongVectorPtr Ap;
			} cg;

		} ppe;

		// 速度修正量
		std::vector<Vector> du;
	
	public:
		struct Exception
		{
			std::string Message;
		};
		
		// @param maxDt 最大時間刻み（出力時間刻み以下など）
		// @param g 重力加速度
		// @param c 音速
		// @param rho 密度
		// @param nu 動粘性係数
		// @param C クーラン数
		// @param r_eByl_0 影響半径と初期粒子間距離の比
		// @param surfaceRatio 自由表面判定の係数
		// @param allowableResidual 圧力方程式の収束判定（許容誤差）
		// @param l_0 初期粒子間距離
		// @param tooNearRatio 過剰接近粒子と判定される距離（初期粒子間距離との比）
		// @param tooNearCoeffcient 過剰接近粒子から受ける修正量の係数
		MpsComputer(
			const double& g,
			const double& rho,
			const double& nu,
			const double& C,
			const double& r_eByl_0,
			const double& surfaceRatio,
			const double& allowableResidual,
			const double& l_0,
			const Particle::List& particles);

		// 初期化する
		void Initialize();

		// 方程式をを解く
		void Solve();

		// 終了処理をする
		void Finalize();
	};
}
#endif