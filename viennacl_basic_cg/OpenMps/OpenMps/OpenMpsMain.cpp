#include "defines.hpp"
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <ctime>
#include <omp.h>
#include <boost/format.hpp>
#include <boost/random.hpp>
#include <boost/timer.hpp>
#include "Particle.hpp"
#include "MpsComputer.hpp"

#ifdef USE_CUDA
#include <cuda_runtime.h>
#endif


// エントリポイント
int main()
{
	using namespace OpenMps;

	const double l_0 =10e-5;
	const double g = 9.8;
	const double rho = 998.20;
	const double nu = 1.004e-6;
	const double C = 0.1;
	const double r_eByl_0 = 2.9;
	const double surfaceRatio = 0.95;
	const double eps = 1e-8;

	// 乱数生成器
	boost::minstd_rand gen(42);
	boost::uniform_real<> dst(0, l_0*0.1);
	boost::variate_generator< boost::minstd_rand&, boost::uniform_real<> > make_rand(gen, dst);


	std::cout << "= BLAS Comparer for OpenMPS =" << std::endl;
	
#ifdef VIENNACL_WITH_OPENMP
	std::cout << "[ViennaCL with OpenMP]" << std::endl;
#endif
#ifdef _OPENMP
	#pragma omp parallel
	{
		#pragma omp master
		{
			std::cout << "OpenMP : " << omp_get_num_threads() << " threads" << std::endl;
		}
	}
#endif

#ifdef VIENNACL_WITH_OPENCL
	std::cout << "[ViennaCL with OpenCL]" << std::endl;
	int i = 0; 
	for(auto platform : viennacl::ocl::get_platforms())
	{
		viennacl::ocl::set_context_platform_index(i, i);

		std::cout << "===" << std::endl
			<< "#" << i++ << std::endl
			<< platform.info() << std::endl;
		for(auto device : platform.devices())
		{
			std::cout << "---" << std::endl
				<< device.info() << std::endl;
		}
	}
	
#endif

#ifdef USE_CUDA
#ifdef USE_VIENNACL
	std::cout << "[ViennaCL with CUDA]" << std::endl;
#else
	std::cout << "[CUBLAS & CUSPARSE]" << std::endl;
#endif
	{
		int deviceCount;
		cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

		for(int dev = 0; dev < deviceCount; dev++)
		{
			cudaSetDevice(dev);

			cudaDeviceProp deviceProp;
			cudaGetDeviceProperties(&deviceProp, dev);
			std::cout << "---" << std::endl
				<< "Name           : " << deviceProp.name << std::endl
				<< "Capability     : " << deviceProp.major << "." << deviceProp.minor << std::endl
				<< "MultiProcessor : " << deviceProp.multiProcessorCount << std::endl;

			
			int driverVersion;
			int runtimeVersion;
			cudaDriverGetVersion(&driverVersion);
			cudaRuntimeGetVersion(&runtimeVersion);
			std::cout
				<< "Driver         : " << driverVersion/1000  << "." << (driverVersion%100)/10 << std::endl
				<< "Runtime        : " << runtimeVersion/1000 << "." << (runtimeVersion%100)/10 << std::endl;

		}
	}
#endif

	std::cout << "Generating particles..." << std::endl;

	Particle::List particles;

	// ダムブレーク環境を作成
	{
		const int L = (int)(12e-2/l_0)*2;
		const int H = (int)(8e-2/l_0)*2;

		// 水を追加
		for(int i = 0; i < L/2; i++)
		{
			for(int j = 0; j < H/1.5; j++)
			{
				double x = i*l_0;
				double y = (j - C*0.1)*l_0;

				double u = make_rand()*C;
				double v = make_rand()*C;

				auto particle = std::shared_ptr<Particle>(new ParticleIncompressibleNewton(x, y, u, v, 0, 0));
				particles.push_back(particle);
			}
		}

		// 床と天井を追加
		for(int i = -1; i < L+1; i++)
		{
			double x = i*l_0;

			// 床
			{
				// 粒子を作成して追加
				auto wall1 = std::shared_ptr<Particle>(new ParticleWall(x, -l_0*1, 0, 0));
				auto dummy1 = std::shared_ptr<Particle>(new ParticleDummy(x, -l_0*2));
				auto dummy2 = std::shared_ptr<Particle>(new ParticleDummy(x, -l_0*3));
				auto dummy3 = std::shared_ptr<Particle>(new ParticleDummy(x, -l_0*4));
				particles.push_back(wall1);
				particles.push_back(dummy1);
				particles.push_back(dummy2);
				particles.push_back(dummy3);
			}
		}

		// 側壁の追加
		for(int j = 0; j < H+1; j++)
		{
			double y = j*l_0;

			// 左壁
			{
				// 粒子を作成して追加
				auto wall1 = std::shared_ptr<Particle>(new ParticleWall(-l_0*1, y, 0, 0));
				auto dummy1 = std::shared_ptr<Particle>(new ParticleDummy(-l_0*2, y));
				auto dummy2 = std::shared_ptr<Particle>(new ParticleDummy(-l_0*3, y));
				auto dummy3 = std::shared_ptr<Particle>(new ParticleDummy(-l_0*4, y));
				particles.push_back(wall1);
				particles.push_back(dummy1);
				particles.push_back(dummy2);
				particles.push_back(dummy3);
			}
			
			// 右壁
			{
				// 粒子を作成して追加
				auto wall1 = std::shared_ptr<Particle>(new ParticleWall((L+0)*l_0, y, 0, 0));
				auto dummy1 = std::shared_ptr<Particle>(new ParticleDummy((L+1)*l_0, y));
				auto dummy2 = std::shared_ptr<Particle>(new ParticleDummy((L+2)*l_0, y));
				auto dummy3 = std::shared_ptr<Particle>(new ParticleDummy((L+3)*l_0, y));
				particles.push_back(wall1);
				particles.push_back(dummy1);
				particles.push_back(dummy2);
				particles.push_back(dummy3);
			}
		}

		// 四隅
		// 側壁の追加
		for(int j = 0; j < 4; j++)
		{
			double y = j*l_0;

			// 左下
			{
				// 粒子を作成して追加
				auto dummy1 = std::shared_ptr<Particle>(new ParticleDummy(-l_0*2, y-4*l_0));
				auto dummy2 = std::shared_ptr<Particle>(new ParticleDummy(-l_0*3, y-4*l_0));
				auto dummy3 = std::shared_ptr<Particle>(new ParticleDummy(-l_0*4, y-4*l_0));
				particles.push_back(dummy1);
				particles.push_back(dummy2);
				particles.push_back(dummy3);
			}
			
			// 右下
			{
				// 粒子を作成して追加
				auto dummy1 = std::shared_ptr<Particle>(new ParticleDummy((L+1)*l_0, y-4*l_0));
				auto dummy2 = std::shared_ptr<Particle>(new ParticleDummy((L+2)*l_0, y-4*l_0));
				auto dummy3 = std::shared_ptr<Particle>(new ParticleDummy((L+3)*l_0, y-4*l_0));
				particles.push_back(dummy1);
				particles.push_back(dummy2);
				particles.push_back(dummy3);
			}
		}
	}

	// 粒子数を表示
	std::cout << particles.size() << " particles" << std::endl;

	
#ifdef VIENNACL_WITH_OPENCL
	for(int i = 0; i < viennacl::ocl::get_platforms().size(); i++)
#endif
	{
#ifdef VIENNACL_WITH_OPENCL
		// プラットフォーム切り替え
		viennacl::ocl::switch_context(i);
		std::cout << "*** [#" << i << "]" << std::endl;
#endif

		// 計算空間の初期化
		MpsComputer computer(
			g,
			rho,
			nu,
			C,
			r_eByl_0,
			surfaceRatio,
			eps,
			l_0,
			particles);
	

		// 開始時間を画面表示
		std::cout << "start! " << std::endl;

		// 計算が終了するまで
		try
		{
			// 開始時間を保存
			boost::timer timer;
			timer.restart();

			// 初期化する
			computer.Initialize();
			auto initalizeTime = timer.elapsed();
			std::cout << "initialize : " << initalizeTime << std::endl;
		
			// 圧力方程式を解く
			computer.Solve();
			auto solveTime = timer.elapsed() - initalizeTime;
			std::cout << "solve      : " << solveTime << std::endl;

			// 計算値を取得する
			computer.Finalize();
			auto finalizeTime = timer.elapsed() - solveTime - initalizeTime;

			// 現在時刻を画面表示
			std::cout
				<< "finalize   : " << finalizeTime << std::endl
				<< "total      : " << initalizeTime + solveTime + finalizeTime << " [s]" << std::endl;
		}
		// 計算で例外があったら
		catch(MpsComputer::Exception ex)
		{
			// エラーメッセージを出して止める
			std::cout << "!!!!ERROR!!!!" << std::endl
				<< ex.Message << std::endl;
		}
	}

	// 終了
	std::cout << "finished" << std::endl;
	system("pause");
	return 0;
}
