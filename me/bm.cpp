#define __CL_ENABLE_EXCEPTIONS

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <windows.h>
#include <CL/cl.hpp>
#include <CL/cl_ext.h>
#include <iostream>
#include <immintrin.h>

using namespace cv;

cl::Platform cl_plt;
cl::Device cl_device;
cl::Context gpu_context;
cl::CommandQueue gpu_queue;
cl::Kernel me_kernel;
cl::Image2D in_image;
cl::Image2D ref_image;
cl::Buffer out_vec;
size_t out_vec_byte_size;
cl_accelerator_intel accelerator;

clGetAcceleratorInfoINTEL_fn p_clGetAcceleratorInfo;
clCreateAcceleratorINTEL_fn p_clCreateAccelrator;

static void
me_cl(unsigned char *in,
      unsigned char *ref,
      int in_step,
      short *vec,
      int col, int row,
      int block_size,
      int search_col,
      int search_row)
{
    cl::size_t<3> copy_origin;
    cl::size_t<3> copy_region;

    LARGE_INTEGER t0, t1, freq;

    copy_origin[0] = 0;
    copy_origin[1] = 0;

    copy_region[0] = col;
    copy_region[1] = row;
    copy_region[2] = 1;

    gpu_queue.enqueueWriteImage(in_image, CL_TRUE, copy_origin, copy_region, 0, 0, in);
    gpu_queue.enqueueWriteImage(ref_image, CL_TRUE, copy_origin, copy_region, 0, 0, ref);

    me_kernel.setArg(0, sizeof(cl_accelerator_intel), &accelerator);
    me_kernel.setArg(1, in_image);
    me_kernel.setArg(2, ref_image);
    me_kernel.setArg(3, sizeof(cl_mem), NULL);
    me_kernel.setArg(4, out_vec);
    me_kernel.setArg(5, sizeof(cl_mem), NULL);

    QueryPerformanceCounter(&t0);
    gpu_queue.enqueueNDRangeKernel(me_kernel,
                                   cl::NDRange(0),
                                   cl::NDRange(col,row));
    gpu_queue.finish();
    QueryPerformanceCounter(&t1);
    long long d = t1.QuadPart - t0.QuadPart;
    QueryPerformanceFrequency(&freq);

    printf("ndrangekernel:%f[ms]\n", (d/(double)freq.QuadPart)*1000.0);

    gpu_queue.enqueueReadBuffer(out_vec, CL_TRUE, 0, out_vec_byte_size, vec);
}


static void
me(unsigned char *in,
   unsigned char *ref,
   int in_step,
   short *vec,
   int col, int row,
   int block_size,
   int search_col,
   int search_row)
{
    int blk_col = col / block_size;
    int blk_row = row / block_size;

#pragma omp parallel for
    for (int bri=0; bri<blk_row; bri++) {
        for (int bci=0; bci<blk_col; bci++) {
            int c = bci * block_size;
            int r = bri * block_size;

            int score = INT_MAX;

            int min0 = 0;
            int min1 = 0;

            for (int ri = -search_row; ri<search_row; ri++) {
                for (int ci = -search_col; ci<search_col; ci++) {
                    int r0 = r+ri;
                    int r1 = r+ri+block_size;
                    int c0 = c+ci;
                    int c1 = c+ci+block_size;

                    if (r0 >= 0 && r1 < row && c0 >= 0 && c1 < col) {
                        int sum = 0;

                        for (int rr0=0; rr0<block_size; rr0++) {
                            for (int cc0=0; cc0<block_size; cc0++) {
                                int rval = ref[(r0+rr0) * in_step + (c0+cc0)];
                                int inval = in[(r+rr0) * in_step + (c+cc0)];

                                int d = abs(rval-inval);

                                sum+=d;
                            }
                        }

                        if (sum < score) {
                            score = sum;
                            min0 = ri;
                            min1 = ci;
                        }
                    }
                }
            }

            vec[(bri * blk_col + bci)*2+0] = min1;
            vec[(bri * blk_col + bci)*2+1] = min0;
        }
    }
}

static __forceinline void
sad16x16_sse2(int *ret0,
              int *ret1,
              unsigned char *p0,
              unsigned char *p1,
              unsigned int step)
{
    __m128i sum2_0;
    __m128i sum2_1;

    __m128i rval;
    __m128i inval;
    __m128i diff0, diff1;

    sum2_0 = _mm_setzero_si128();
    sum2_1 = _mm_setzero_si128();

    for (int i=0; i<16; i++) {
        rval = _mm_loadu_si128((__m128i*)p0);
        inval = _mm_loadu_si128((__m128i*)p1);
        diff0 = _mm_sad_epu8(rval, inval);

        rval = _mm_loadu_si128((__m128i*)(p0+1));
        inval = _mm_loadu_si128((__m128i*)(p1+1));
        diff1 = _mm_sad_epu8(rval, inval);

        sum2_0 = _mm_add_epi16(sum2_0, diff0);
        sum2_1 = _mm_add_epi16(sum2_1, diff1);

        p0 += step;
        p1 += step;
    }

    int sum0 = _mm_cvtsi128_si32(sum2_0);
    sum0 = (sum0&0xffff) + (sum0>>16);

    int sum1 = _mm_cvtsi128_si32(sum2_1);
    sum1 = (sum1&0xffff) + (sum1>>16);

    *ret0 = sum0;
    *ret1 = sum1;
}

static void
me_16_sse2(unsigned char *in,
           unsigned char *ref,
           int in_step,
           short *vec,
           int col, int row,
           int block_size,
           int search_col,
           int search_row)
{
    block_size = 16;
    int blk_col = col / block_size;
    int blk_row = row / block_size;

#pragma omp parallel for
    for (int bri=0; bri<blk_row; bri++) {
        for (int bci=0; bci<blk_col; bci++) {
            int c = bci * block_size;
            int r = bri * block_size;

            int score = INT_MAX;

            int min0 = 0;
            int min1 = 0;

            for (int ri = -search_row; ri<search_row; ri++) {
                for (int ci = -search_col; ci<search_col; ci+=2) {
                    int r0 = r+ri;
                    int r1 = r+ri+block_size;
                    int c0 = c+ci;
                    int c1 = c+ci+block_size;

                    if (r0 >= 0 && r1 < row && c0 >= 0 && c1 < col) {
                        int sum0;
                        int sum1;

                        sad16x16_sse2(&sum0,
                                      &sum1,
                                      &ref[r0 * in_step + c0],
                                      &in[r * in_step + c],
                                      in_step);

                        if (sum0 < score) {
                            score = sum0;
                            min0 = ri;
                            min1 = ci;
                        }

                        if (sum1 < score) {
                            score = sum1;
                            min0 = ri;
                            min1 = ci+1;
                        }
                    }
                }
            }

            vec[(bri * blk_col + bci)*2+0] = min1;
            vec[(bri * blk_col + bci)*2+1] = min0;
        }
    }
}

static int
setup_opencl(int width, int height,
             int blk_size,
             int radius)
{
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);

    if (platforms.size() == 0) {
        puts("cl platform not found");
        return -1;
    }

    bool found = false;

    for (int pid=0; pid<(int)platforms.size(); pid++) {
        std::string name;
        platforms[pid].getInfo(CL_PLATFORM_VENDOR, &name);

        if (strncmp(name.c_str(), "Intel", 5) == 0) {
            std::vector<cl::Device> gpus;

            try {
                platforms[pid].getDevices(CL_DEVICE_TYPE_GPU, &gpus);
            } catch (cl::Error err) {
            }

            for (int di=0; di<(int)gpus.size(); di++) {
                std::string ext;
                gpus[di].getInfo(CL_DEVICE_EXTENSIONS, &ext);

                if (strstr(ext.c_str(), "cl_intel_accelerator") != NULL) {
                    found = true;

                    cl_plt = platforms[pid];
                    cl_device = gpus[di];

                    std::string dev_name;

                    gpus[di].getInfo(CL_DEVICE_NAME, &dev_name);

                    printf("%s:%s\n", name.c_str(), dev_name.c_str());
                }
            }

        }
    }

    if (!found) {
        puts("cl_intel_accelerator extension not found");
        return -1;
    }

    p_clGetAcceleratorInfo = (clGetAcceleratorInfoINTEL_fn)
        clGetExtensionFunctionAddressForPlatform(cl_plt(), "clGetAcceleratorInfoINTEL");
    p_clCreateAccelrator = (clCreateAcceleratorINTEL_fn)
        clGetExtensionFunctionAddressForPlatform(cl_plt(), "clCreateAcceleratorINTEL");

    if (p_clGetAcceleratorInfo == NULL || p_clCreateAccelrator == NULL) {
        puts("cl_intel_accelerator extension not found");
        return -1;
    }

    gpu_context = cl::Context(cl_device);
    gpu_queue = cl::CommandQueue(gpu_context);

    cl_motion_estimation_desc_intel me_desc = {0};

    switch (blk_size) {
    case 4:
        me_desc.mb_block_type = CL_ME_MB_TYPE_4x4_INTEL;
        break;
    case 8:
        me_desc.mb_block_type = CL_ME_MB_TYPE_8x8_INTEL;
        break;
    case 16:
        me_desc.mb_block_type = CL_ME_MB_TYPE_16x16_INTEL;
        break;
    default:
        printf("blk_size=%d is not supported\n", blk_size);
        return -1;
    }

    switch (radius) {
    case 2:
        me_desc.search_path_type = CL_ME_SEARCH_PATH_RADIUS_2_2_INTEL;
        break;

    case 4:
        me_desc.search_path_type = CL_ME_SEARCH_PATH_RADIUS_4_4_INTEL;
        break;

    case 16:
        me_desc.search_path_type = CL_ME_SEARCH_PATH_RADIUS_16_12_INTEL;
        break;

    default:
        printf("radius=%d is not supported\n", radius);
        return -1;
    }


    me_desc.subpixel_mode = CL_ME_SUBPIXEL_MODE_INTEGER_INTEL;
    me_desc.sad_adjust_mode = CL_ME_SAD_ADJUST_MODE_NONE_INTEL;

    cl_int err;

    accelerator = p_clCreateAccelrator(gpu_context(),
                                       CL_ACCELERATOR_TYPE_MOTION_ESTIMATION_INTEL,
                                       sizeof(me_desc),
                                       &me_desc,
                                       &err);

    if (accelerator == NULL) {
        puts("clCreateAccelerator failed");
        return -1;
    }

    std::vector<cl::Device> gpu_devs;

    gpu_devs.push_back(cl_device);

    cl::Program me_prog(gpu_context, gpu_devs, "block_motion_estimate_intel");
    me_kernel = cl::Kernel(me_prog, "block_motion_estimate_intel");

    return 0;
}

static int
main2(int argc, char **argv)
{
    int block_size = 16;
    int search_col = 16;
    int search_row = 12;

    if (argc > 1) {
        block_size = atoi(argv[1]);
    }
    if (argc > 2) {
        int search = atoi(argv[2]);
        if (search == 2) {
            search_row = search_col = 2;
        } else if (search == 4) {
            search_row = search_col = 4;
        } else if (search == 16) {
            search_row = 12;
            search_col = 16;
        } else {
            search_row = search_col = search;
        }
    }

    VideoCapture cap;

    if (argc > 3) {
        cap.open(argv[3]);
    } else {
        cap.open(0);
    }
    Mat in, gray, prev, result;

    short *vec = NULL;

    cap>>in;
    if (in.empty()) {
        exit(1);
    }

    printf("blocksize=%d, search=%dx%d, input=%dx%d\n", block_size, search_col, search_row, (int)in.cols, (int)in.rows);

    int cl = 0;
    if (argc > 4) {
        cl = -1;
    }

    if (cl == 0) {
        cl = setup_opencl(in.cols, in.rows, block_size, search_col);
    }

    cvtColor(in, gray, COLOR_BGR2GRAY);
    int vec_col = in.cols/block_size;
    int vec_row = in.rows/block_size;
    vec = new short[(vec_col * vec_row)*2];

    if (cl == 0) {
        out_vec_byte_size = vec_col*vec_row*2*sizeof(unsigned short);
        out_vec = cl::Buffer(gpu_context, CL_MEM_WRITE_ONLY, out_vec_byte_size);
        in_image = cl::Image2D(gpu_context,
                               CL_MEM_READ_WRITE,
                               cl::ImageFormat(CL_R, CL_UNORM_INT8),
                               in.cols, in.rows, 0);
        ref_image = cl::Image2D(gpu_context,
                                CL_MEM_READ_WRITE,
                                cl::ImageFormat(CL_R, CL_UNORM_INT8),
                                in.cols, in.rows, 0);
    }

    for (;;) {
        swap(gray, prev);

        cap >> in;
        if (in.empty()) {
            break;
        }

        cvtColor(in, gray, COLOR_BGR2GRAY);

        LARGE_INTEGER t0, t1, freq;

        QueryPerformanceCounter(&t0);
        if (cl == 0) {
            me_cl(gray.ptr(0),
                  prev.ptr(0),
                  gray.step1(0),
                  vec,
                  in.cols, in.rows,
                  block_size,
                  search_col, search_row);
        } else {
            if (block_size == 16 && ((search_col&3) == 0)) {
                me_16_sse2(gray.ptr(0),
                           prev.ptr(0),
                           gray.step1(0),
                           vec,
                           in.cols, in.rows,
                           block_size,
                           search_col, search_row);
            } else {
                me(gray.ptr(0),
                   prev.ptr(0),
                   gray.step1(0),
                   vec,
                   in.cols, in.rows,
                   block_size,
                   search_col, search_row);
            }
        }
        QueryPerformanceCounter(&t1);

        long long d = t1.QuadPart - t0.QuadPart;
        QueryPerformanceFrequency(&freq);

        printf("%f[ms]\n", (d/(double)freq.QuadPart)*1000.0);

        in.copyTo(result);

        {
            int blk_col = result.cols / block_size;
            int blk_row = result.rows / block_size;

            for (int ri=0; ri<blk_row; ri++) {
                for (int ci=0; ci<blk_col; ci++) {
                    int bx = ci*block_size + block_size/2;
                    int by = ri*block_size + block_size/2;

                    int vecx;
                    int vecy;
                    if (cl == 0) {
                        vecx = vec[(ri*vec_col + ci)*2 + 0];
                        vecx = -((vecx)>>2);

                        vecy = vec[(ri*vec_col + ci)*2 + 1];
                        vecy = -((vecy)>>2);
                    } else {
                        vecx = -vec[(ri*vec_col + ci)*2 + 0]*1;
                        vecy = -vec[(ri*vec_col + ci)*2 + 1]*1;
                    }

                    {
                        Point p1(bx, by);
                        Point p2(bx+vecx, by+vecy);
                        line(result, p1, p2, Scalar(0,0,255));
                    }


                    {
                        Point p1(bx-1, by-1);
                        Point p2(bx+1, by+1);
                        rectangle(result, p1, p2,
                                  Scalar(0,0,255));
                    }

                }
            }
        }

        namedWindow("out");

        imshow("out", result);

        char c = waitKey(1);
        if (c == 033) {
            break;
        }
    }


}


int
main(int argc, char **argv)
{
    try {
        main2(argc, argv);
    } catch (cl::Error err) {
        std::cerr
            << "ERROR: "
            << err.what()
            << "("
            << err.err()
            << ")"
            << std::endl;
    }
}


