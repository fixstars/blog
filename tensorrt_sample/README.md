# Caffe & TensorRT 推論サンプルコード

## Prerequisite
* 本サンプルコードは NVIDIA 版 Caffe (https://github.com/NVIDIA/caffe) がインストールされていることを前提としています。ビルド手順は適宜ドキュメントを参照して下さい。
* 以下の環境変数を設定してください
  * export CAFFE_ROOT=/path/to/caffe  # 適切に設定願います
  * export LD_LIBRARY_PATH=$CAFFE_ROOT/build/lib:$LD_LIBRARY_PATH
* `$CAFFE_ROOT/data/ilsvrc12/get_ilsvrc_aux.sh` が実行済みであること

## ビルド手順
基本的には、デフォルトの状態で `make` すれば、実行ファイルがビルドされます。
ただし、環境によってビルドエラーになる場合があります。その際は、以下の手順に従ってください。

### OpenCV3系をインストールしている場合
`Makefile` に `LIBRARIES += oepncv_imgcodecs` を追加してください。

### 「`TIFFxxxx@LIBTIFF_4.0` に対する定義されていない参照」エラーが出る
`Makefile` に `LIBRARIES += tiff` を追加してください。
環境によって要・不要が分かれるため、 `Makefile` には書かれていません。

## 推論コードの種類
* `inference_caffe`: NVCaffe による推論コード。バッチサイズは1のみがサポートされています。
* `inference_tensorrt`: TensorRT による FP32 推論コード
* `inference_fp16`: TensorRT による FP16 推論コード

TensorRT による推論コードは、性能検証用に任意のバッチサイズをサポートしていますが、推論可能な画像は1枚のみです。

## 推論実行方法
各推論プログラムは、以下のパラメータをとります。
1. 学習済み Caffe モデルの prototxt
2. 学習済み Caffe モデルの caffemodel
3. 平均画素の binaryproto
4. ラベル一覧
5. 推論対象画像ファイル
6. バッチサイズ (TensorRT 版のみ)

推論対象画像は、カラー画像であれば OpenCV で読める範囲で任意の形式が使えます。
プログラム内部で 224x224 にリサイズするので、前処理は特に必要ありません。

プログラムの実行例は以下のとおりです。
```
$ ./inference_tensorrt  \
    ~/googlenet.prototxt \
    ~/googlenet.caffemodel \
    $CAFFE_ROOT/data/ilsvrc12/imagenet_mean.binaryproto \
    $CAFFE_ROOT/data/ilsvrc12/synset_words.txt \
    $CAFFE_ROOT/examples/images/cat.jpg
```
