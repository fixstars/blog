#include <NvInfer.h>
#include <NvCaffeParser.h>
#include <caffe/caffe.hpp>
#include <cuda_runtime_api.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <nvToolsExt.h>

#ifdef GPU_LIMITED_PROFILING
# include <cuda_profiler_api.h>
#endif // GPU_LIMITED_PROFILING

#define NUM_WARMUP (10)

using namespace nvinfer1;
using namespace nvcaffeparser1;
using std::string;

const std::string OUTPUT_BLOB_NAME = "prob";
unsigned int g_batch_size;

/* get elapsed usec */
double get_elapsed_usec(struct timespec &start, struct timespec &end) {
    double ret = (end.tv_sec - start.tv_sec) * 1e6;
    ret += ((end.tv_nsec < start.tv_nsec) ?
            end.tv_nsec + 1e9 - start.tv_nsec : end.tv_nsec - start.tv_nsec) * 1e-3;
    return ret;
}

/* Pair (label, confidence) representing a prediction. */
typedef std::pair<string, float> Prediction;

// Logger for TensorRT
class Logger : public ILogger {
public:
    void log(ILogger::Severity severity, const char* msg) override
    {
#ifndef SHOW_TENSORRT_LOG
        if (severity == Severity::kINFO) return;
#endif // SHOW_TENSORRT_LOG
        std::cout << msg << std::endl;
    }
};

template<typename Dtype>
class Blob {
public:
    Blob(const Dims& dims);
    // TODO: impl destructor
    int channels();
    int height();
    int width();
    int nBytes();
    Dtype* cpu_data();
    void* gpu_data();
private:
    int num_bytes_;
    Dims dims_;
    Dtype* bufCPU_;
    void* bufGPU_;
};

template<typename Dtype>
class Net {
public:
    Net(const string& model_file,
        const string& trained_file,
        const std::vector<std::string>& outputs);
    // TODO: impl destructor
    std::vector< Blob<Dtype>* > input_blobs();
    std::vector< Blob<Dtype>* > output_blobs();
    int num_inputs();
    int num_outputs();
    void Forward();

private:
    Logger logger_;
    ICudaEngine* engine_;
    IExecutionContext* context_;
    std::vector< Blob<Dtype> > blobs_;
    std::vector<int> inputIdx_;
    std::vector<int> outputIdx_;
};

class Classifier {
public:
    Classifier(const string& model_file,
               const string& trained_file,
               const string& mean_file,
               const string& label_file,
               const std::vector<std::string>& outputs);
    // TODO: impl destructor
    std::vector<Prediction> Classify(const cv::Mat& img, int N = 5);

private:
    void SetMean(const string& mean_file);

    std::vector<float> Predict(const cv::Mat& img);

    void WrapInputLayer(std::vector<cv::Mat>* input_channels);

    void Preprocess(const cv::Mat& img,
                    std::vector<cv::Mat>* input_channels);

private:
    Net<float>* net_;
    cv::Size input_geometry_;
    int num_channels_;
    cv::Mat mean_;
    std::vector<string> labels_;
};

template<typename Dtype>
Blob<Dtype>::Blob(const Dims& dims) {
    CHECK((dims.nbDims == 1) || (dims.nbDims == 3));
    dims_ = dims;

    // calculate buffer size
    num_bytes_ = sizeof(Dtype);
    for (int i=0; i<dims.nbDims; ++i)
        num_bytes_ *= dims.d[i];
    num_bytes_ *= g_batch_size;

    // allocate host & device memory
    bufCPU_ = static_cast<Dtype*>(malloc(num_bytes_));
    CHECK(bufCPU_);
    CHECK(cudaMalloc(&bufGPU_, num_bytes_) == cudaSuccess);
}

template<typename Dtype>
int Blob<Dtype>::channels() {
    return dims_.d[0];
}

template<typename Dtype>
int Blob<Dtype>::height() {
    // assume dims.nbDims == 3
    return dims_.d[1];
}

template<typename Dtype>
int Blob<Dtype>::width() {
    // assume dims.nbDims == 3
    return dims_.d[2];
}

template<typename Dtype>
int Blob<Dtype>::nBytes() {
    return num_bytes_;
}

template<typename Dtype>
Dtype* Blob<Dtype>::cpu_data() {
    return bufCPU_;
}

template<typename Dtype>
void* Blob<Dtype>::gpu_data() {
    return bufGPU_;
}

template<typename Dtype>
Net<Dtype>::Net(const string& model_file,
         const string& trained_file,
         const std::vector<std::string>& outputs) {
    // initialize TensorRT network optimizer
    IBuilder* builder = createInferBuilder(logger_);
    CHECK(builder);

    // check if FP16 is supported
    CHECK(builder->platformHasFastFp16());

    // parser caffe model
    INetworkDefinition* network = builder->createNetwork();
    ICaffeParser* parser = createCaffeParser();
    CHECK(network);
    CHECK(parser);

    const IBlobNameToTensor* blobNameToTensor = parser->parse(model_file.c_str(),
                                                              trained_file.c_str(),
                                                              *network,
                                                              DataType::kHALF);
    CHECK(blobNameToTensor);

    // mark output of network (caffe model doesn't have output info)
    for (auto& s : outputs) {
        std::cout << "[INFO] marking blob " << s << " as output." << std::endl;
        CHECK(blobNameToTensor->find(s.c_str()));
        network->markOutput(*blobNameToTensor->find(s.c_str()));
    }

    // build TensorRT engine
    CHECK(g_batch_size);
    builder->setMaxBatchSize(g_batch_size);
    builder->setMaxWorkspaceSize(16 << 20);
    builder->setDebugSync(true);
    builder->setHalf2Mode(true);

    ICudaEngine* engine = builder->buildCudaEngine(*network);
    CHECK(engine);

    // destroy used objects
    network->destroy();
    parser->destroy();

    // serialize the engine and close TensorRT optimizer
    IHostMemory* modelStream = engine->serialize();
    engine->destroy();
    builder->destroy();
    shutdownProtobufLibrary();

    // initialize TensorRT network executor runtime
    IRuntime* infer = createInferRuntime(logger_);
    CHECK(infer);

    engine_ = infer->deserializeCudaEngine(modelStream->data(),
                                           modelStream->size(),
                                           nullptr);
    CHECK(engine_);

    // create TensorRT execution context
    context_ = engine_->createExecutionContext();
    CHECK(context_);

    // allocate in/out blobs
    for (int bi = 0; bi < engine_->getNbBindings(); ++bi) {
        if (engine_->bindingIsInput(bi)) {
            inputIdx_.push_back(bi);
        } else {
            outputIdx_.push_back(bi);
        }
        Dims dims = engine_->getBindingDimensions(bi);
        blobs_.push_back(Blob<float>(dims));
    }
}

template<typename Dtype>
std::vector< Blob<Dtype>* > Net<Dtype>::input_blobs() {
    std::vector< Blob<Dtype>* > blobs;
    for (int i = 0; i < inputIdx_.size(); ++i) {
        blobs.push_back(&blobs_[inputIdx_[i]]);
    }
    return blobs;
}

template<typename Dtype>
std::vector< Blob<Dtype>* > Net<Dtype>::output_blobs() {
    std::vector< Blob<Dtype>* > blobs;
    for (int i = 0; i < outputIdx_.size(); ++i) {
        blobs.push_back(&blobs_[outputIdx_[i]]);
    }
    return blobs;
}

template<typename Dtype>
void Net<Dtype>::Forward() {
    cudaStream_t stream;
    nvtxRangeId_t r0 = nvtxRangeStart("Create CUDA stream");
    CHECK(cudaStreamCreate(&stream) == cudaSuccess);
    nvtxRangeEnd(r0);

    // H2D memcpy of input
    nvtxRangeId_t r1 = nvtxRangeStart("Enq H->D");
    for (int i=0; i<num_inputs(); ++i) {
        Blob<Dtype>* b = &blobs_[inputIdx_[i]];
        CHECK(cudaMemcpyAsync(b->gpu_data(), (void *)b->cpu_data(),
                              b->nBytes(), cudaMemcpyHostToDevice, stream) == cudaSuccess);
    }
    nvtxRangeEnd(r1);

    // enqueue forward
    nvtxRangeId_t r2 = nvtxRangeStart("Host Process");
    std::vector<void*> buffers(blobs_.size());
    for (int i=0; i<blobs_.size(); ++i) {
        buffers[i] = blobs_[i].gpu_data();
    }
    nvtxRangeEnd(r2);

    nvtxRangeId_t r3 = nvtxRangeStart("Enq Execution");
    context_->enqueue(g_batch_size, &buffers[0], stream, nullptr);
    nvtxRangeEnd(r3);

    // D2H memcpy of output
    nvtxRangeId_t r4 = nvtxRangeStart("Enq D->H");
    for (int i=0; i<num_outputs(); ++i) {
        Blob<Dtype>* b = &blobs_[outputIdx_[i]];
        CHECK(cudaMemcpyAsync((void *)b->cpu_data(), b->gpu_data(),
                              b->nBytes(), cudaMemcpyDeviceToHost, stream) == cudaSuccess);
    }
    nvtxRangeEnd(r4);

    // wait for result
    nvtxRangeId_t r5 = nvtxRangeStart("Waiting...");
    cudaStreamSynchronize(stream);
    nvtxRangeEnd(r5);

    // clean up
    nvtxRangeId_t r6 = nvtxRangeStart("Clean up");
    cudaStreamDestroy(stream);
    nvtxRangeEnd(r6);
}

template<typename Dtype>
int Net<Dtype>::num_inputs() {
    return static_cast<int>(inputIdx_.size());
}

template<typename Dtype>
int Net<Dtype>::num_outputs() {
    return static_cast<int>(outputIdx_.size());
}

Classifier::Classifier(const string& model_file,
                       const string& trained_file,
                       const string& mean_file,
                       const string& label_file,
                       const std::vector<std::string>& outputs) {

    /* Load the network. */
    net_ = new Net<float>(model_file, trained_file, outputs);

    CHECK_EQ(net_->num_inputs(), 1) << "Network should have exactly one input.";
    CHECK_EQ(net_->num_outputs(), 1) << "Network should have exactly one output.";

    Blob<float>* input_layer = net_->input_blobs()[0];
    num_channels_ = input_layer->channels();
    CHECK(num_channels_ == 3 || num_channels_ == 1)
        << "Input layer should have 1 or 3 channels.";
    input_geometry_ = cv::Size(input_layer->width(), input_layer->height());

    /* Load the binaryproto mean file. */
    SetMean(mean_file);

    /* Load labels. */
    std::ifstream labels(label_file.c_str());
    CHECK(labels) << "Unable to open labels file " << label_file;
    string line;
    while (std::getline(labels, line))
        labels_.push_back(string(line));

    Blob<float>* output_layer = net_->output_blobs()[0];
    CHECK_EQ(labels_.size(), output_layer->channels())
        << "Number of labels is different from the output layer dimension.";
}

static bool PairCompare(const std::pair<float, int>& lhs,
                        const std::pair<float, int>& rhs) {
    return lhs.first > rhs.first;
}

/* Return the indices of the top N values of vector v. */
static std::vector<int> Argmax(const std::vector<float>& v, int N) {
    std::vector<std::pair<float, int> > pairs;
    for (size_t i = 0; i < v.size(); ++i)
        pairs.push_back(std::make_pair(v[i], i));
    std::partial_sort(pairs.begin(), pairs.begin() + N, pairs.end(), PairCompare);

    std::vector<int> result;
    for (int i = 0; i < N; ++i)
        result.push_back(pairs[i].second);
    return result;
}

/* Return the top N predictions. */
std::vector<Prediction> Classifier::Classify(const cv::Mat& img, int N) {
    std::vector<float> output = Predict(img);

    N = std::min<int>(labels_.size(), N);
    std::vector<int> maxN = Argmax(output, N);
    std::vector<Prediction> predictions;
    for (int i = 0; i < N; ++i) {
        int idx = maxN[i];
        predictions.push_back(std::make_pair(labels_[idx], output[idx]));
    }

    return predictions;
}

/* Load the mean file in binaryproto format. */
void Classifier::SetMean(const string& mean_file) {
    caffe::BlobProto blob_proto;
    caffe::ReadProtoFromBinaryFileOrDie(mean_file.c_str(), &blob_proto);

    /* Convert from BlobProto to TBlob<float> */
    caffe::TBlob<float> mean_blob;
    mean_blob.FromProto(blob_proto);
    CHECK_EQ(mean_blob.channels(), num_channels_)
        << "Number of channels of mean file doesn't match input layer.";

    /* The format of the mean file is planar 32-bit float BGR or grayscale. */
    std::vector<cv::Mat> channels;
    float* data = mean_blob.mutable_cpu_data();
    for (int i = 0; i < num_channels_; ++i) {
        /* Extract an individual channel. */
        cv::Mat channel(mean_blob.height(), mean_blob.width(), CV_32FC1, data);
        channels.push_back(channel);
        data += mean_blob.height() * mean_blob.width();
    }

    /* Merge the separate channels into a single image. */
    cv::Mat mean;
    cv::merge(channels, mean);

    /* Compute the global mean pixel value and create a mean image
     * filled with this value. */
    cv::Scalar channel_mean = cv::mean(mean);
    mean_ = cv::Mat(input_geometry_, mean.type(), channel_mean);
}

std::vector<float> Classifier::Predict(const cv::Mat& img) {
    Blob<float>* input_layer = net_->input_blobs()[0];

    std::vector<cv::Mat> input_channels;
    WrapInputLayer(&input_channels);

    Preprocess(img, &input_channels);

    /* warm up */
    for (int i=0; i<NUM_WARMUP; ++i) {
        struct timespec t0, t1;
        clock_gettime(CLOCK_MONOTONIC, &t0);

        nvtxRangeId_t warmup = nvtxRangeStart("Warmup run");
        net_->Forward();
        nvtxRangeEnd(warmup);

        clock_gettime(CLOCK_MONOTONIC, &t1);
        double elapsed_usec = get_elapsed_usec(t0, t1);
        std::cout << std::fixed << std::setprecision(3)
                  << "Warmup inference time(us): " << elapsed_usec << std::endl;
    }
    sleep(1);

    /* inference with time measurement */
    struct timespec t0, t1;
    clock_gettime(CLOCK_MONOTONIC, &t0);
#ifdef GPU_LIMITED_PROFILING
    cudaProfilerStart();
#endif // GPU_LIMITED_PROFILING
    nvtxRangeId_t exec_range = nvtxRangeStart("Forward");
    net_->Forward();
    nvtxRangeEnd(exec_range);
#ifdef GPU_LIMITED_PROFILING
    cudaProfilerStop();
#endif // GPU_LIMITED_PROFILING
    clock_gettime(CLOCK_MONOTONIC, &t1);

    /* print elapsed time */
    double elapsed_usec = get_elapsed_usec(t0, t1);
    std::cout << std::fixed << std::setprecision(3)
              << "inference time(us): " << elapsed_usec << std::endl;

    /* Copy the output layer to a std::vector */
    Blob<float>* output_layer = net_->output_blobs()[0];
    const float* begin = output_layer->cpu_data();
    const float* end = begin + output_layer->channels();
    return std::vector<float>(begin, end);
}

/* Wrap the input layer of the network in separate cv::Mat objects
 * (one per channel). This way we save one memcpy operation and we
 * don't need to rely on cudaMemcpy2D. The last preprocessing
 * operation will write the separate channels directly to the input
 * layer. */
void Classifier::WrapInputLayer(std::vector<cv::Mat>* input_channels) {
    Blob<float>* input_layer = net_->input_blobs()[0];

    int width = input_layer->width();
    int height = input_layer->height();
    float* input_data = input_layer->cpu_data();
    for (int i = 0; i < input_layer->channels(); ++i) {
        cv::Mat channel(height, width, CV_32FC1, input_data);
        input_channels->push_back(channel);
        input_data += width * height;
    }
}

void Classifier::Preprocess(const cv::Mat& img,
                            std::vector<cv::Mat>* input_channels) {
    /* Convert the input image to the input image format of the network. */
    cv::Mat sample;
    if (img.channels() == 3 && num_channels_ == 1)
        cv::cvtColor(img, sample, cv::COLOR_BGR2GRAY);
    else if (img.channels() == 4 && num_channels_ == 1)
        cv::cvtColor(img, sample, cv::COLOR_BGRA2GRAY);
    else if (img.channels() == 4 && num_channels_ == 3)
        cv::cvtColor(img, sample, cv::COLOR_BGRA2BGR);
    else if (img.channels() == 1 && num_channels_ == 3)
        cv::cvtColor(img, sample, cv::COLOR_GRAY2BGR);
    else
        sample = img;

    cv::Mat sample_resized;
    if (sample.size() != input_geometry_)
        cv::resize(sample, sample_resized, input_geometry_);
    else
        sample_resized = sample;

    cv::Mat sample_float;
    if (num_channels_ == 3)
        sample_resized.convertTo(sample_float, CV_32FC3);
    else
        sample_resized.convertTo(sample_float, CV_32FC1);

    cv::Mat sample_normalized;
    cv::subtract(sample_float, mean_, sample_normalized);

    /* This operation will write the separate BGR planes directly to the
     * input layer of the network because it is wrapped by the cv::Mat
     * objects in input_channels. */
    cv::split(sample_normalized, *input_channels);

    CHECK(reinterpret_cast<float*>(input_channels->at(0).data)
          == net_->input_blobs()[0]->cpu_data())
        << "Input channels are not wrapping the input layer of the network.";
}

int main(int argc, char** argv) {
    if ((argc != 6) && (argc != 7)) {
        std::cerr << "Usage: " << argv[0]
                  << " deploy.prototxt network.caffemodel"
                  << " mean.binaryproto labels.txt img.jpg (batch size)" << std::endl;
        return 1;
    }

    ::google::InitGoogleLogging(argv[0]);

    string model_file   = argv[1];
    string trained_file = argv[2];
    string mean_file    = argv[3];
    string label_file   = argv[4];
    g_batch_size = (argc == 7) ? atoi(argv[6]) : 1;
    Classifier classifier(model_file, trained_file, mean_file, label_file,
                          std::vector<std::string>{OUTPUT_BLOB_NAME});

    string file = argv[5];

    std::cout << "---------- Prediction for "
              << file << " ----------" << std::endl;
    std::cout << "Batch size: " << g_batch_size << std::endl;

    cv::Mat img = cv::imread(file, -1);
    CHECK(!img.empty()) << "Unable to decode image " << file;
    std::vector<Prediction> predictions = classifier.Classify(img);

    /* Print the top N predictions. */
    for (size_t i = 0; i < predictions.size(); ++i) {
        Prediction p = predictions[i];
        std::cout << std::fixed << std::setprecision(4) << p.second << " - \""
                  << p.first << "\"" << std::endl;
    }
}
