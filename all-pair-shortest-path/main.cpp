#include <iostream>
#include <algorithm>
#include <random>
#include <array>
#include <cstring>

static const int MIN_N = (1 << 6);
static const int MAX_N = (1 << 14);
static const int TRIAL_COUNT = 5;

double solve_reference(float *out, const float *in, int n);
double solve_avx_recursive(float *out, const float *in, int n);
double solve_avx_recursive_gemm(float *out, const float *in, int n);
double solve_gpu_naive(float *out, const float *in, int n);
double solve_gpu_recursive(float *out, const float *in, int n);

//-----------------------------------------------------------------------------
// Benchmark
//-----------------------------------------------------------------------------
void generate(float *out, int n){
	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
	for(int i = 0; i < n * n; ++i){
		out[i] = distribution(generator);
	}
	for(int i = 0; i < n; ++i){
		out[i * n + i] = 0.0f;
	}
}

void validate(const float *a, const float *b, int n){
#ifdef ENABLE_VALIDATION
	float max_error = 0.0f;
	for(int i = 0; i < n; ++i){
		for(int j = 0; j < n; ++j){
			max_error = std::max<float>(max_error, fabs(a[i * n + j] - b[i * n + j]));
		}
	}
	std::cout << "    (max error: " << max_error << ")" << std::endl;
#endif
}

template <typename F>
void benchmark(const char *name, F func, float *dst, const float *src, int n){
	std::array<double, TRIAL_COUNT> durations;
	for(int i = 0; i < TRIAL_COUNT; ++i){
		durations[i] = func(dst, src, n);
	}
	std::sort(durations.begin(), durations.end());
	const auto duration = durations[TRIAL_COUNT / 2];
	const auto gflops = 2.0 * n * n * n / duration * 1e-6;
	std::cout << name << ": " << duration << " [ms], " << gflops << " [GFLOPS]" << std::endl;
}

int main(){
	for(int n = MIN_N; n <= MAX_N; n += n){
		std::cout << "--------------------" << std::endl;
		std::cout << " N = " << n << std::endl;
		std::cout << "--------------------" << std::endl;

		std::vector<float> input(n * n);
		std::vector<float> ref_output(n * n);
		std::vector<float> tar_output(n * n);
		generate(input.data(), n);

		if(n < MAX_N){
			benchmark("Reference     ", solve_reference, ref_output.data(), input.data(), n);
		}

		if(n < MAX_N){
			benchmark("AVX recursive ", solve_avx_recursive, tar_output.data(), input.data(), n);
			validate(ref_output.data(), tar_output.data(), n);
		}

		benchmark("AVX recur+gemm", solve_avx_recursive_gemm, tar_output.data(), input.data(), n);
		validate(ref_output.data(), tar_output.data(), n);

		if(n < MAX_N){
			benchmark("GPU naive     ", solve_gpu_naive, tar_output.data(), input.data(), n);
			validate(ref_output.data(), tar_output.data(), n);
		}

		benchmark("GPU recursive ", solve_gpu_recursive, tar_output.data(), input.data(), n);
		validate(ref_output.data(), tar_output.data(), n);
	}
	return 0;
}

