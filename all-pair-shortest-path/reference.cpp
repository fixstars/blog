#include <chrono>
#include <algorithm>
#include <cstring>

double solve_reference(float *out, const float *in, int n){
	using double_milliseconds =
		std::chrono::duration<double, std::ratio<1, 1000>>;
	memcpy(out, in, sizeof(float) * n * n);

	const auto begin_time = std::chrono::steady_clock::now();
#pragma omp parallel
	for(int k = 0; k < n; ++k){
#pragma omp for
		for(int i = 0; i < n; ++i){
			for(int j = 0; j < n; ++j){
				out[i * n + j] = std::min(
					out[i * n + j], out[i * n + k] + out[k * n + j]);
			}
		}
	}
	const auto end_time = std::chrono::steady_clock::now();
	return double_milliseconds(end_time - begin_time).count();
}

