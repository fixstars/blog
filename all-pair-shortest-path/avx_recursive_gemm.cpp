#include <chrono>
#include <algorithm>
#include <cstring>
#include <immintrin.h>

namespace {

//---------------------------------------------------------------------------
// Naive Floyd-Warshall algorithm
//---------------------------------------------------------------------------
void naive(float *out, int n, int stride){
#pragma omp single
	for(int k = 0; k < n; ++k){
		for(int i = 0; i < n; ++i){
			const __m256 v_in0_ik = _mm256_broadcast_ss(out + i * stride + k);
			for(int j = 0; j < n; j += 8){
				const __m256 v_out_ij = _mm256_loadu_ps(out + i * stride + j);
				const __m256 v_in1_kj = _mm256_loadu_ps(out + k * stride + j);
				const __m256 v_minval = _mm256_min_ps(
					v_out_ij, _mm256_add_ps(v_in0_ik, v_in1_kj));
				_mm256_storeu_ps(out + i * stride + j, v_minval);
			}
		}
	}
}

//---------------------------------------------------------------------------
// Tropical SGEMM
//---------------------------------------------------------------------------
static const int GEMM_BLOCK_SIZE = 64;
void tropical_gemm(float *out, const float *in0, const float *in1, int n, int stride){
	float c_in0[GEMM_BLOCK_SIZE * GEMM_BLOCK_SIZE] __attribute__((aligned(32)));
	float c_in1[GEMM_BLOCK_SIZE * GEMM_BLOCK_SIZE] __attribute__((aligned(32)));
#pragma omp for
	for(int ci = 0; ci < n; ci += GEMM_BLOCK_SIZE){
		for(int cj = 0; cj < n; cj += GEMM_BLOCK_SIZE){
			float *c_out = out + ci * stride + cj;
			for(int ck = 0; ck < n; ck += GEMM_BLOCK_SIZE){
				for(int i = 0; i < GEMM_BLOCK_SIZE; ++i){
					for(int j = 0; j < GEMM_BLOCK_SIZE; j += 8){
						const int c_offset = i * GEMM_BLOCK_SIZE + j;
						const auto v_in0 = _mm256_loadu_ps(in0 + (ci + i) * stride + (ck + j));
						_mm256_store_ps(c_in0 + c_offset, v_in0);
						const auto v_in1 = _mm256_loadu_ps(in1 + (ck + i) * stride + (cj + j));
						_mm256_store_ps(c_in1 + c_offset, v_in1);
					}
				}
				for(int ri = 0; ri < GEMM_BLOCK_SIZE; ri += 2){
					for(int rj = 0; rj < GEMM_BLOCK_SIZE; rj += 32){
						__m256 o00 = _mm256_loadu_ps(c_out + (ri + 0) * stride + (rj +  0));
						__m256 o01 = _mm256_loadu_ps(c_out + (ri + 0) * stride + (rj +  8));
						__m256 o02 = _mm256_loadu_ps(c_out + (ri + 0) * stride + (rj + 16));
						__m256 o03 = _mm256_loadu_ps(c_out + (ri + 0) * stride + (rj + 24));
						__m256 o10 = _mm256_loadu_ps(c_out + (ri + 1) * stride + (rj +  0));
						__m256 o11 = _mm256_loadu_ps(c_out + (ri + 1) * stride + (rj +  8));
						__m256 o12 = _mm256_loadu_ps(c_out + (ri + 1) * stride + (rj + 16));
						__m256 o13 = _mm256_loadu_ps(c_out + (ri + 1) * stride + (rj + 24));
						for(int rk = 0; rk < GEMM_BLOCK_SIZE; rk += 2){
							const float *in0_line = c_in0 + ri * GEMM_BLOCK_SIZE + rk;
							const float *in1_line = c_in1 + rk * GEMM_BLOCK_SIZE + rj;
							const __m256 i1_00 = _mm256_load_ps(in1_line +  0);
							const __m256 i1_01 = _mm256_load_ps(in1_line +  8);
							const __m256 i1_02 = _mm256_load_ps(in1_line + 16);
							const __m256 i1_03 = _mm256_load_ps(in1_line + 24);
							const __m256 i0_00 = _mm256_broadcast_ss(in0_line);
							const __m256 i0_10 = _mm256_broadcast_ss(in0_line + GEMM_BLOCK_SIZE);
							o00 = _mm256_min_ps(o00, _mm256_add_ps(i1_00, i0_00));
							o01 = _mm256_min_ps(o01, _mm256_add_ps(i1_01, i0_00));
							o02 = _mm256_min_ps(o02, _mm256_add_ps(i1_02, i0_00));
							o03 = _mm256_min_ps(o03, _mm256_add_ps(i1_03, i0_00));
							o10 = _mm256_min_ps(o10, _mm256_add_ps(i1_00, i0_10));
							o11 = _mm256_min_ps(o11, _mm256_add_ps(i1_01, i0_10));
							o12 = _mm256_min_ps(o12, _mm256_add_ps(i1_02, i0_10));
							o13 = _mm256_min_ps(o13, _mm256_add_ps(i1_03, i0_10));
							in0_line += 1;
							in1_line += GEMM_BLOCK_SIZE;
							const __m256 i1_10 = _mm256_load_ps(in1_line +  0);
							const __m256 i1_11 = _mm256_load_ps(in1_line +  8);
							const __m256 i1_12 = _mm256_load_ps(in1_line + 16);
							const __m256 i1_13 = _mm256_load_ps(in1_line + 24);
							const __m256 i0_01 = _mm256_broadcast_ss(in0_line);
							const __m256 i0_11 = _mm256_broadcast_ss(in0_line + GEMM_BLOCK_SIZE);
							o00 = _mm256_min_ps(o00, _mm256_add_ps(i1_10, i0_01));
							o01 = _mm256_min_ps(o01, _mm256_add_ps(i1_11, i0_01));
							o02 = _mm256_min_ps(o02, _mm256_add_ps(i1_12, i0_01));
							o03 = _mm256_min_ps(o03, _mm256_add_ps(i1_13, i0_01));
							o10 = _mm256_min_ps(o10, _mm256_add_ps(i1_10, i0_11));
							o11 = _mm256_min_ps(o11, _mm256_add_ps(i1_11, i0_11));
							o12 = _mm256_min_ps(o12, _mm256_add_ps(i1_12, i0_11));
							o13 = _mm256_min_ps(o13, _mm256_add_ps(i1_13, i0_11));
						}
						 _mm256_storeu_ps(c_out + (ri + 0) * stride + (rj +  0), o00);
						 _mm256_storeu_ps(c_out + (ri + 0) * stride + (rj +  8), o01);
						 _mm256_storeu_ps(c_out + (ri + 0) * stride + (rj + 16), o02);
						 _mm256_storeu_ps(c_out + (ri + 0) * stride + (rj + 24), o03);
						 _mm256_storeu_ps(c_out + (ri + 1) * stride + (rj +  0), o10);
						 _mm256_storeu_ps(c_out + (ri + 1) * stride + (rj +  8), o11);
						 _mm256_storeu_ps(c_out + (ri + 1) * stride + (rj + 16), o12);
						 _mm256_storeu_ps(c_out + (ri + 1) * stride + (rj + 24), o13);
					}
				}
			}
		}
	}
}

void recur(float *out, int n, int stride){
	const int THRESHOLD = GEMM_BLOCK_SIZE;
	if(n <= THRESHOLD){
		naive(out, n, stride);
	}else{
		const int h = n / 2, d00 = 0, d01 = h, d10 = h * stride, d11 = d01 + d10;
		recur(out + d00, h, stride);
		tropical_gemm(out + d01, out + d00, out + d01, h, stride);
		tropical_gemm(out + d10, out + d10, out + d00, h, stride);
		tropical_gemm(out + d11, out + d10, out + d01, h, stride);
		recur(out + d11, h, stride);
		tropical_gemm(out + d10, out + d11, out + d10, h, stride);
		tropical_gemm(out + d01, out + d01, out + d11, h, stride);
		tropical_gemm(out + d00, out + d01, out + d10, h, stride);
	}
}

}

double solve_avx_recursive_gemm(float *out, const float *in, int n){
	using double_milliseconds =
		std::chrono::duration<double, std::ratio<1, 1000>>;
	memcpy(out, in, sizeof(float) * n * n);

	const auto begin_time = std::chrono::steady_clock::now();
#pragma omp parallel
	recur(out, n, n);
	const auto end_time = std::chrono::steady_clock::now();
	return double_milliseconds(end_time - begin_time).count();
}

