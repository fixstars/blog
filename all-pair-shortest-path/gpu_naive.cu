namespace {

static const int BLOCK_SIZE = 16;

__global__ void naive_kernel(float *out, int n, int k){
	const int i = blockIdx.y * blockDim.y + threadIdx.y;
	const int j = blockIdx.x * blockDim.x + threadIdx.x;
	out[i * n + j] = fmin(out[i * n + j], out[i * n + k] + out[k * n + j]);
}

}

double solve_gpu_naive(float *out, const float *in, int n){
	float *dout = nullptr;
	cudaMalloc(&dout, sizeof(float) * n * n);
	cudaEvent_t begin_event, end_event;
	cudaEventCreate(&begin_event);
	cudaEventCreate(&end_event);

	cudaMemcpy(dout, in, sizeof(float) * n * n, cudaMemcpyHostToDevice);
	cudaEventRecord(begin_event, 0);
	for(int k = 0; k < n; ++k){
		const dim3 gdim(n / BLOCK_SIZE, n / BLOCK_SIZE);
		const dim3 bdim(BLOCK_SIZE, BLOCK_SIZE);
		naive_kernel<<<gdim, bdim>>>(dout, n, k);
	}
	cudaEventRecord(end_event, 0);
	cudaMemcpy(out, dout, sizeof(float) * n * n, cudaMemcpyDeviceToHost);

	float elapsed_time_ms = 0.0f;
	cudaEventElapsedTime(&elapsed_time_ms, begin_event, end_event);

	cudaEventDestroy(begin_event);
	cudaEventDestroy(end_event);
	cudaFree(dout);
	return elapsed_time_ms;
}

