namespace {

//---------------------------------------------------------------------------
// Naive Floyd-Warshall algortithm on single CTA
//---------------------------------------------------------------------------
static const int NAIVE_BLOCK_SIZE_X = 64;
static const int NAIVE_BLOCK_SIZE_Y = 8;

__global__ void naive_kernel(float *out, int n, int stride){
	const int i0 = blockIdx.y * blockDim.y + threadIdx.y;
	const int j  = blockIdx.x * blockDim.x + threadIdx.x;
	const int ws = NAIVE_BLOCK_SIZE_X + 1;
	__shared__ float work[NAIVE_BLOCK_SIZE_X * ws];
	for(int i = i0; i < n; i += blockDim.y){
		work[i * ws + j] = out[i * stride + j];
	}
	__syncthreads();
	for(int k = 0; k < n; ++k){
		for(int i = i0; i < n; i += blockDim.y){
			work[i * ws + j] = fmin(
				work[i * ws + j], work[i * ws + k] + work[k * ws + j]);
		}
		__syncthreads();
	}
	for(int i = i0; i < n; i += blockDim.y){
		out[i * stride + j] = work[i * ws + j];
	}
}

void call_naive(float *out, int n, int stride){
	const dim3 gdim(n / NAIVE_BLOCK_SIZE_X, n / NAIVE_BLOCK_SIZE_Y);
	const dim3 bdim(NAIVE_BLOCK_SIZE_X, NAIVE_BLOCK_SIZE_Y);
	naive_kernel<<<gdim, bdim>>>(out, n, stride);
}

//---------------------------------------------------------------------------
// Tropical SGEMM
//   - 4x4 blocking on registers
//   - 64x32 blocking on shared memory
//---------------------------------------------------------------------------
static const int GEMM_BLOCK_SIZE = 256; // 16x16
static const int GEMM_BLOCK_WIDTH = 64, GEMM_BLOCK_HEIGHT = 64;
static const int GEMM_SMEM_BLOCK_WIDTH = 64, GEMM_SMEM_BLOCK_HEIGHT = 32;

__device__ __forceinline__
float4 read_out(const float *out, int stride, int row){
	const int local_y = threadIdx.x >> 4, local_x = threadIdx.x & 0x0f;
	const int i = blockIdx.y * GEMM_BLOCK_HEIGHT + local_y * 4 + row;
	const int j = blockIdx.x * GEMM_BLOCK_WIDTH  + local_x * 4;
	return *(const float4 *)(out + i * stride + j);
}
__device__ __forceinline__
void write_out(float *out, int stride, int row, float4 x){
	const int local_y = threadIdx.x >> 4, local_x = threadIdx.x & 0x0f;
	const int i = blockIdx.y * GEMM_BLOCK_HEIGHT + local_y * 4 + row;
	const int j = blockIdx.x * GEMM_BLOCK_WIDTH  + local_x * 4;
	*(float4 *)(out + i * stride + j) = x;
}

__device__ __forceinline__
void read_in0(float *s_in0, const float *in0, int stride, int k){
	const int local_y = threadIdx.x >> 3, local_x = threadIdx.x & 0x07;
	const int y = blockIdx.y * GEMM_BLOCK_HEIGHT + local_y;
	const int x = k + local_x * 4;
#pragma unroll
	for(int t = 0; t < GEMM_SMEM_BLOCK_WIDTH; t += 32){
		float4 v = *(const float4 *)(in0 + (y + t) * stride + x);
		*(float4 *)(s_in0 + (t + local_y) * GEMM_SMEM_BLOCK_HEIGHT + local_x * 4) = v;
	}
}
__device__ __forceinline__
void read_in1(float *s_in1, const float *in1, int stride, int k){
	const int local_y = threadIdx.x >> 4, local_x = threadIdx.x & 0x0f;
	const int y = k + local_y;
	const int x = blockIdx.x * GEMM_BLOCK_WIDTH + local_x * 4;
#pragma unroll
	for(int t = 0; t < GEMM_SMEM_BLOCK_HEIGHT; t += 16){
		const int s_offset = (t + local_y) * GEMM_SMEM_BLOCK_WIDTH + local_x * 4;
		*(float4 *)(s_in1 + s_offset) = *(const float4 *)(in1 + (y + t) * stride + x);
	}
}

__device__ __forceinline__
float4 read_shared_in0(const float *s_in, int t, int offset){
	return *(const float4 *)(s_in + offset * GEMM_SMEM_BLOCK_HEIGHT + t);
}
__device__ __forceinline__
float4 read_shared_in1(const float *s_in, int t, int offset){
	return *(const float4 *)(s_in + t * GEMM_SMEM_BLOCK_WIDTH + offset);
}

__device__ __forceinline__
void update(float &x, float y, float z){
	x = fmin(x, fmin(y, z));
}

__launch_bounds__(GEMM_BLOCK_SIZE, 4)
__global__ void tropical_sgemm_kernel(
	float *out, const float *in0, const float *in1, int n, int stride)
{
	float4 out_x = read_out(out, stride, 0);
	float4 out_y = read_out(out, stride, 1);
	float4 out_z = read_out(out, stride, 2);
	float4 out_w = read_out(out, stride, 3);

	__shared__ float s_in0[GEMM_SMEM_BLOCK_WIDTH * GEMM_SMEM_BLOCK_HEIGHT];
	__shared__ float s_in1[GEMM_SMEM_BLOCK_WIDTH * GEMM_SMEM_BLOCK_HEIGHT];
	const int in0_offset = (threadIdx.x >> 4) * 4;
	const int in1_offset = (threadIdx.x & 15) * 4;
	for(int k = 0; k < n; k += GEMM_SMEM_BLOCK_HEIGHT){
		read_in0(s_in0, in0, stride, k);
		read_in1(s_in1, in1, stride, k);
		__syncthreads();
#pragma unroll
		for(int t = 0; t < GEMM_SMEM_BLOCK_HEIGHT; t += 4){
			const float4 in0_x = read_shared_in0(s_in0, t, in0_offset + 0);
			const float4 in0_y = read_shared_in0(s_in0, t, in0_offset + 1);
			const float4 in0_z = read_shared_in0(s_in0, t, in0_offset + 2);
			const float4 in0_w = read_shared_in0(s_in0, t, in0_offset + 3);
			const float4 in1_x = read_shared_in1(s_in1, t + 0, in1_offset);
			const float4 in1_y = read_shared_in1(s_in1, t + 1, in1_offset);
			const float4 in1_z = read_shared_in1(s_in1, t + 2, in1_offset);
			const float4 in1_w = read_shared_in1(s_in1, t + 3, in1_offset);
			update(out_x.x, in0_x.x + in1_x.x, in0_x.y + in1_y.x);
			update(out_x.y, in0_x.x + in1_x.y, in0_x.y + in1_y.y);
			update(out_x.z, in0_x.x + in1_x.z, in0_x.y + in1_y.z);
			update(out_x.w, in0_x.x + in1_x.w, in0_x.y + in1_y.w);
			update(out_y.x, in0_y.x + in1_x.x, in0_y.y + in1_y.x);
			update(out_y.y, in0_y.x + in1_x.y, in0_y.y + in1_y.y);
			update(out_y.z, in0_y.x + in1_x.z, in0_y.y + in1_y.z);
			update(out_y.w, in0_y.x + in1_x.w, in0_y.y + in1_y.w);
			update(out_z.x, in0_z.x + in1_x.x, in0_z.y + in1_y.x);
			update(out_z.y, in0_z.x + in1_x.y, in0_z.y + in1_y.y);
			update(out_z.z, in0_z.x + in1_x.z, in0_z.y + in1_y.z);
			update(out_z.w, in0_z.x + in1_x.w, in0_z.y + in1_y.w);
			update(out_w.x, in0_w.x + in1_x.x, in0_w.y + in1_y.x);
			update(out_w.y, in0_w.x + in1_x.y, in0_w.y + in1_y.y);
			update(out_w.z, in0_w.x + in1_x.z, in0_w.y + in1_y.z);
			update(out_w.w, in0_w.x + in1_x.w, in0_w.y + in1_y.w);
			update(out_x.x, in0_x.z + in1_z.x, in0_x.w + in1_w.x);
			update(out_x.y, in0_x.z + in1_z.y, in0_x.w + in1_w.y);
			update(out_x.z, in0_x.z + in1_z.z, in0_x.w + in1_w.z);
			update(out_x.w, in0_x.z + in1_z.w, in0_x.w + in1_w.w);
			update(out_y.x, in0_y.z + in1_z.x, in0_y.w + in1_w.x);
			update(out_y.y, in0_y.z + in1_z.y, in0_y.w + in1_w.y);
			update(out_y.z, in0_y.z + in1_z.z, in0_y.w + in1_w.z);
			update(out_y.w, in0_y.z + in1_z.w, in0_y.w + in1_w.w);
			update(out_z.x, in0_z.z + in1_z.x, in0_z.w + in1_w.x);
			update(out_z.y, in0_z.z + in1_z.y, in0_z.w + in1_w.y);
			update(out_z.z, in0_z.z + in1_z.z, in0_z.w + in1_w.z);
			update(out_z.w, in0_z.z + in1_z.w, in0_z.w + in1_w.w);
			update(out_w.x, in0_w.z + in1_z.x, in0_w.w + in1_w.x);
			update(out_w.y, in0_w.z + in1_z.y, in0_w.w + in1_w.y);
			update(out_w.z, in0_w.z + in1_z.z, in0_w.w + in1_w.z);
			update(out_w.w, in0_w.z + in1_z.w, in0_w.w + in1_w.w);
		}
		__syncthreads();
	}

	write_out(out, stride, 0, out_x);
	write_out(out, stride, 1, out_y);
	write_out(out, stride, 2, out_z);
	write_out(out, stride, 3, out_w);
}

void call_sgemm(float *out, const float *in0, const float *in1, int n, int stride){
	const dim3 gdim(n / GEMM_BLOCK_WIDTH, n / GEMM_BLOCK_HEIGHT);
	const dim3 bdim(GEMM_BLOCK_SIZE);
	tropical_sgemm_kernel<<<gdim, bdim>>>(out, in0, in1, n, stride);
}

//---------------------------------------------------------------------------
// Recursive Floyd-Warshall algorithm
//---------------------------------------------------------------------------
void recur(float *out, int n, int stride){
	const int THRESHOLD = NAIVE_BLOCK_SIZE_X;
	if(n <= THRESHOLD){
		call_naive(out, n, stride);
	}else{
		const int h = n / 2, d00 = 0, d01 = h, d10 = h * stride, d11 = d01 + d10;
		recur(out + d00, h, stride);
		call_sgemm(out + d01, out + d00, out + d01, h, stride);
		call_sgemm(out + d10, out + d10, out + d00, h, stride);
		call_sgemm(out + d11, out + d10, out + d01, h, stride);
		recur(out + d11, h, stride);
		call_sgemm(out + d10, out + d11, out + d10, h, stride);
		call_sgemm(out + d01, out + d01, out + d11, h, stride);
		call_sgemm(out + d00, out + d01, out + d10, h, stride);
	}
}

}

double solve_gpu_recursive(float *out, const float *in, int n){
	float *dout = nullptr;
	cudaMalloc(&dout, sizeof(float) * n * n);
	cudaEvent_t begin_event, end_event;
	cudaEventCreate(&begin_event);
	cudaEventCreate(&end_event);

	cudaMemcpy(dout, in, sizeof(float) * n * n, cudaMemcpyHostToDevice);
	cudaEventRecord(begin_event, 0);
	recur(dout, n, n);
	cudaEventRecord(end_event, 0);
	cudaMemcpy(out, dout, sizeof(float) * n * n, cudaMemcpyDeviceToHost);

	float elapsed_time_ms = 0.0f;
	cudaEventElapsedTime(&elapsed_time_ms, begin_event, end_event);

	cudaEventDestroy(begin_event);
	cudaEventDestroy(end_event);
	cudaFree(dout);
	return elapsed_time_ms;
}

