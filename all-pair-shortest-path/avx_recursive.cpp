#include <chrono>
#include <algorithm>
#include <cstring>
#include <immintrin.h>

namespace {

void naive(float *out, const float *in0, const float *in1, int n, int stride){
	for(int k = 0; k < n; ++k){
#pragma omp for
		for(int i = 0; i < n; ++i){
			const __m256 v_in0_ik = _mm256_broadcast_ss(in0 + i * stride + k);
			for(int j = 0; j < n; j += 8){
				const __m256 v_out_ij = _mm256_loadu_ps(out + i * stride + j);
				const __m256 v_in1_kj = _mm256_loadu_ps(in1 + k * stride + j);
				const __m256 v_minval = _mm256_min_ps(
					v_out_ij, _mm256_add_ps(v_in0_ik, v_in1_kj));
				_mm256_storeu_ps(out + i * stride + j, v_minval);
			}
		}
	}
}

void recur(float *out, const float *in0, const float *in1, int n, int stride){
	const int THRESHOLD = 256;
	if(n <= THRESHOLD){
		naive(out, in0, in1, n, stride);
	}else{
		const int h = n / 2, d00 = 0, d01 = h, d10 = h * stride, d11 = d01 + d10;
		recur(out + d00, in0 + d00, in1 + d00, h, stride);
		recur(out + d01, in0 + d00, in1 + d01, h, stride);
		recur(out + d10, in0 + d10, in1 + d00, h, stride);
		recur(out + d11, in0 + d10, in1 + d01, h, stride);
		recur(out + d11, in0 + d11, in1 + d11, h, stride);
		recur(out + d10, in0 + d11, in1 + d10, h, stride);
		recur(out + d01, in0 + d01, in1 + d11, h, stride);
		recur(out + d00, in0 + d01, in1 + d10, h, stride);
	}
}

}

double solve_avx_recursive(float *out, const float *in, int n){
	using double_milliseconds =
		std::chrono::duration<double, std::ratio<1, 1000>>;
	memcpy(out, in, sizeof(float) * n * n);

	const auto begin_time = std::chrono::steady_clock::now();
#pragma omp parallel
	recur(out, out, out, n, n);
	const auto end_time = std::chrono::steady_clock::now();
	return double_milliseconds(end_time - begin_time).count();
}

