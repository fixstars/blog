#define _GNU_SOURCE

#include <x86intrin.h> 
#include <stdio.h>
#include <unistd.h>
#include <sched.h>
#include <pthread.h>

#define MAX_THREADS 256

static int tsc_valid[MAX_THREADS];
long long tsc_offset[MAX_THREADS];

static void *
calib_thread(void *vp)
{
    volatile int *p = (int*)vp;

    while (*p == 0)
        ;

    unsigned int id;
    unsigned long long val;

    val = __rdtscp(&id);

    tsc_offset[id] = val;
    tsc_valid[id] = 1;
    return NULL;
}

static void *
calib_thread0(void *vp)
{
    volatile int *p = (int*)vp;
    usleep(1000);

    *p = 1;

    unsigned int id;
    unsigned long long val;

    val = __rdtscp(&id);

    tsc_offset[id] = val;
    tsc_valid[id] = 1;

    return NULL;
}

struct rdtscp_val {
    unsigned long long real_val;
    unsigned long long adjusted_val;
};

static inline struct rdtscp_val
RDTSCP(void)
{
    unsigned long long val;
    unsigned int id;
    struct rdtscp_val ret;

    val = __rdtscp(&id);

    ret.real_val = val;
    ret.adjusted_val = val - tsc_offset[id];

    return ret;
}

static void
calibrate_tsc(void)
{
    int syncval = 0;

    int num_thread = sysconf(_SC_NPROCESSORS_ONLN);

    pthread_attr_t *attrs = malloc(sizeof (pthread_attr_t) * num_thread);
    pthread_t *threads = malloc(sizeof(pthread_t) * num_thread);
    int i;

    for (i=0; i<num_thread; i++) {
        cpu_set_t set;

        CPU_ZERO(&set);
        CPU_SET(i, &set);

        pthread_attr_init(&attrs[i]);
        pthread_attr_setaffinity_np(&attrs[i], sizeof(cpu_set_t), &set);
    }

    for (i=0; i<num_thread; i++) {
        if (i==0) {
            pthread_create(&threads[i], &attrs[i],
                           calib_thread0, &syncval);
        } else {
            pthread_create(&threads[i], &attrs[i],
                           calib_thread, &syncval);
        }
    }

    for (i=0; i<num_thread; i++) {
        pthread_join(threads[0], NULL);
        printf("%2d: %lld %d\n", i, tsc_offset[i], (int)tsc_valid[i]);
    }

    free(threads);
    free(attrs);
}


int
main()
{
    struct rdtscp_val tsc0;
    struct rdtscp_val tsc1;

    calibrate_tsc();

    tsc0 = RDTSCP();
    usleep(100000);
    tsc1 = RDTSCP();

    printf("real tsc=%lld, adjusted tsc=%lld\n",
           tsc1.real_val - tsc0.real_val,
           tsc1.adjusted_val - tsc0.adjusted_val);

    return 0;
}
