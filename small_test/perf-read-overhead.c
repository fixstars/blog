#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <x86intrin.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

#define N 2048

static int
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags )
{
    int ret;

    ret = syscall( __NR_perf_event_open, hw_event, pid, cpu,
                   group_fd, flags );
    return ret;
}

unsigned long long delta[N];

int main()
{
    struct perf_event_attr attr;
    int perf_fd;
    long long val0, val1;
    long long tsc0, tsc1;
    int i;

    memset(&attr, 0, sizeof(attr));

    attr.type = PERF_TYPE_HARDWARE;
    attr.size = sizeof(attr);
    attr.config = PERF_COUNT_HW_CPU_CYCLES;

    perf_fd = perf_event_open(&attr, 0, -1, -1, 0);
    if (perf_fd == -1) {
        perror("perf_event_open");
        exit(1);
    }

    for (i=-4; i<N; i++) {
        tsc0 = __rdtsc();
        read(perf_fd, &val0, sizeof(val0));
        tsc1 = __rdtsc();

        if (i>=0) {
            delta[i] = tsc1 - tsc0;
        }
    }

    unsigned long long sum = 0;
    unsigned long long max = 0;
    unsigned long long min = ULLONG_MAX;
    for (i=0; i<N; i++) {
        unsigned long long d = delta[i];

        sum += d;
        if (d > max) { max = d; }
        if (d < min) { min = d; }
    }

    printf("ave=%f, max=%llu, min=%llu\n",
           sum/(double)N, max, min);

    close(perf_fd);
}
