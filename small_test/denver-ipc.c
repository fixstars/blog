#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <time.h>
#include <sys/mman.h>

#define N 16384
char data[2][N];

int data_large[1024*1024*128];
float fp_data_large[1024*1024*128];

char *program_copy;
size_t program_size;
int clk_fd, inst_fd;

double usec() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);

    return (ts.tv_sec*1000000.0) + (ts.tv_nsec/1000.0);
}

static int
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags )
{
    int ret;

    ret = syscall( __NR_perf_event_open, hw_event, pid, cpu,
                   group_fd, flags );
    return ret;
}

typedef void (*test_func_t)(int iter);
typedef void (*noarg_t)(void);

noarg_t noarg_func_ptr;

void ret2();
void ret3();
void ret4();
void ret5();
void ret6();
void ret7();
void ret8();
void ret9();

void ret(){
    noarg_func_ptr = ret2;
    return ;
} 
void ret2(){
    noarg_func_ptr = ret3;
    return ;
} 
void ret3(){
    noarg_func_ptr = ret4;
    return ;
} 
void ret4(){
    noarg_func_ptr = ret5;
    return ;
} 
void ret5(){
    noarg_func_ptr = ret6;
    return ;
} 
void ret6(){
    noarg_func_ptr = ret7;
    return ;
} 
void ret7(){
    noarg_func_ptr = ret8;
    return ;
} 
void ret8(){
    noarg_func_ptr = ret9;
    return ;
} 
void ret9(){
    noarg_func_ptr = ret;
    return ;
} 

double start_usec;

long long dclk[1024];
long long dinst[1024];
double usec_result[1024];

static void
test(const char *name, test_func_t test, int niter)
{
    long long clk0, clk1;
    long long inst0, inst1;

    double func_start_usec = usec();

    for (int iter=0; iter<niter; iter++) {
        read(clk_fd, &clk0, sizeof(clk0));
        read(inst_fd, &inst0, sizeof(inst0));

        test(iter);

        read(clk_fd, &clk1, sizeof(clk1));
        read(inst_fd, &inst1, sizeof(inst1));

        dclk[iter] = clk1-clk0;
        dinst[iter] = inst1-inst0;

        usec_result[iter] = usec();
    }

    for (int iter=0; iter<niter; iter++) {
        long long dc = dclk[iter];
        long long di = dinst[iter];
        double us = usec_result[iter];

        printf("%20s-%2d: %f, clk=%lld, inst=%lld, abs=%f,%f[msec]\n",
               name,
               iter,
               di/(double)dc,
               dc,
               di,
               (us - start_usec)/1000, (us - func_start_usec)/1000);
    }
}

#define ITER8(A) A A A A A A A A
#define ITER16(A) ITER8(A) ITER8(A)
#define ITER32(A) ITER16(A) ITER16(A)
#define ITER64(A) ITER32(A) ITER32(A)


#define ITER8_N(A,OFF) A(0+OFF) A(1+OFF) A(2+OFF) A(3+OFF) A(4+OFF) A(5+OFF) A(6+OFF) A(7+OFF)
#define ITER16_N(A,OFF) ITER8(A,0+OFF) ITER8(A,8+OFF)
#define ITER32_N(A,OFF) ITER16_N(A,0+OFF) ITER16(A,16+OFF)


static void
nop_func(int iter)
{
    int *p = data_large;
    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__ ("nop\n\t":[ptr]"+r"(p)::"memory", "s0"););
    }
}

void __attribute__((noinline, noclone)) ret_func() {
    return ;
}

static void
nop_call_func(int iter)
{
    int *p = data_large;
    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__ ("bl ret_func":::"x30") ;);
    }
}


static void
unroll_func(int iter)
{
    int v0=0, v1=1, v2=3;
    int *p = data_large;
    for (int i=0; i<100000; i++) {
        __asm__ __volatile__("ldr %w[v0], [%[ptr]]\n\t"
                             "add %w[v0], %w[v0], %w[v1]\n\t"
                             "add %w[v0], %w[v0], %w[v2]\n\t"
                             "str %w[v0], [%[ptr]], 4\n\t"
                             :[v0]"+r"(v0),
                              [v1]"+r"(v1),
                              [v2]"+r"(v2),
                              [ptr]"+r"(p));
    }
}

static void
fp_nop_func(int iter)
{
    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__ ("fmov s0, 1.0e+0\n\t"
                                     "fadd s0, s0, s0\n\t"
                                     :
                                     :
                                     :"memory", "s0");
               
            );
    }
}

static void
int_nop_func(int iter)
{
    int v1 = 1;
    int vsum = 0;
    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__ ("mov %[v1], 1\n\t"
                                     "add %[vsum], %[vsum], %[v1]\n\t"
                                     :[v1]"+r"(v1), [vsum]"+r"(vsum));
               
            );
    }
    data[0][0] = vsum;
}


static void
int_nop_clear_func(int iter)
{
    int val=0;
    __asm__ __volatile__ ("mov %[val], 1\n\t"
                          :[val]"+r"(val));


    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__ ("add %[val], %[val], %[val]\n\t"
                                     :[val]"+r"(val)

                   ););
        __asm__ __volatile__ ("mov %[val], 0\n\t"
                              :[val]"+r"(val));
    }
}


static void
cond_nop_func(int iter)
{
    __asm__ __volatile__("mov w0, 1" ::: "w0");
    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__ ("add w0, w0, w0\n\tcmp w0, 4\n\tbeq 1f\n\t1:\n\t":::"w0", "memory"););
        __asm__ __volatile__("mov w0, 1" ::: "w0");
    }
}



static void
add_func(int iter)
{
    int val = 0, val2=2;
    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__ ("add %[reg], %[reg], 1\n\t"
                                     "sub %[reg], %[reg], 1\n\t"
                                     :[reg]"+r"(val),
                                      [reg2]"+r"(val2)
                   );
            );
    }

    data[0][0] = val;
}

static void
indirect_call(int iter)
{
    int val = 0;

    for (int i=0; i<100000; i++) {
        ITER32(noarg_func_ptr(););
    }
}

static void
loop_int_dep(int iter)
{
    int val=0, val2=0;

    for (int j=0; j<128; j++) {
        int *ptr = (int*)(data_large+0);
        int *ptr2 = (int*)(data_large+128);

        if (iter > 8 && iter < 24) {
            ptr2 = (int*)(data_large+0);
        }

        for (int i=0; i<4096; i++) {
            ITER32(__asm__ __volatile__("ldr %w[reg], [%[ptr]]\n\t"
                                        "add %w[reg], %w[reg], 1\n\t"
                                        "str %w[reg], [%[ptr]]\n\t"

                                        "ldr %w[reg2], [%[ptr2]]\n\t"
                                        "add %w[reg2], %w[reg2], 1\n\t"
                                        "str %w[reg2], [%[ptr2]]\n\t"
                                        :[reg]"+r"(val),
                                         [reg2]"+r"(val2),
                                         [ptr]"+r"(ptr),
                                         [ptr2]"+r"(ptr2)););
        }
    }
}

static void
load_iter1(int iter)
{
    int *ptr = (int*)(data[0]);
    int *ptr2 = (int*)(data[0]);

    int val=0;

    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__("ldr %w[reg], [%[ptr]]\n\t"
                                    "add %w[reg], %w[reg], 1\n\t"
                                    "str %w[reg], [%[ptr2]]\n\t"
                                    :[reg]"+r"(val),
                                     [ptr]"+r"(ptr),
                                     [ptr2]"+r"(ptr2)););
    }

    data[0][0] = (uintptr_t)val;
    data[0][1] = (uintptr_t)ptr;
    data[0][2] = (uintptr_t)ptr2;
}

static void
loop_fp(int iter)
{
    float *ptr = (float*)(fp_data_large);
    float *ptr2 = (float*)(fp_data_large);

    float reg=0;

    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__("ldr %s[reg], [%[ptr]]\n\t"
                                    "fadd %s[reg], %s[reg], %s[reg]\n\t"
                                    "str %s[reg], [%[ptr]]\n\t"
                                    :[reg]"+w"(reg),
                                     [ptr]"+r"(ptr),
                                     [ptr2]"+r"(ptr2));

            );
    }
}

static void
loop_fp_inc(int iter)
{
    float *ptr = (float*)(fp_data_large);
    float *ptr2 = (float*)(fp_data_large);

    float reg=0;

    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__("ldr %s[reg], [%[ptr]]\n\t"
                                    "fadd %s[reg], %s[reg], %s[reg]\n\t"
                                    "str %s[reg], [%[ptr]]\n\t"
                                    :[reg]"+w"(reg),
                                     [ptr]"+r"(ptr),
                                     [ptr2]"+r"(ptr2));
               ptr++;
            );
    }
}


static void
loop_fp_dep(int iter)
{
    
    float reg=0;

    for (int j=0; j<128; j++) {
        float *ptr = (float*)(fp_data_large);
        float *ptr2 = (float*)(fp_data_large+128);
        if (iter > 8 && iter < 24) {
            ptr2 = (float*)fp_data_large;
        }

        for (int i=0; i<1024; i++) {
            ITER32(__asm__ __volatile__("ldr %s[reg], [%[ptr]]\n\t"
                                        "fadd %s[reg], %s[reg], %s[reg]\n\t"
                                        "str %s[reg], [%[ptr]]\n\t"

                                        "ldr %s[reg], [%[ptr2]]\n\t"
                                        "fadd %s[reg], %s[reg], %s[reg]\n\t"
                                        "str %s[reg], [%[ptr2]]\n\t"

                                        :[reg]"+w"(reg),
                                         [ptr]"+r"(ptr),
                                        [ptr2]"+r"(ptr2));
                );
        }
    }
}



static void
load_reg1(int iter)
{
    int *ptr = (int*)(data[0]);

    int val=0;

    for (int i=0; i<100000; i++) {
        ITER32(__asm__ __volatile__("ldr %w[reg], [%[ptr]]\n\t"
                                    "add %w[reg], %w[reg], 1\n\t"
                                    "str %w[reg], [%[ptr]]\n\t"
                                    :[reg]"+r"(val),
                                     [ptr]"+r"(ptr)
                   ););
    }

    data[0][0] = (uintptr_t)val;
    data[0][1] = (uintptr_t)ptr;
}


static void
ipc7(int iter)
{

    int val=0, val2=0, val3=0, val4=0, val5=0, val6=0, val7 = 0;
    float fval=0, fval2=0, fval3=0;

    for (int i=0; i<64*1024; i++) {
        int *ptr = (int*)(data_large);
        int *ptr2 = (int*)(data_large+1024+16);

        ITER32(
            __asm__ __volatile__("ldr %w[reg], [%[ptr]], 8\n\t"
                                 "ldr %w[reg2], [%[ptr2]], 8\n\t"
                                 "add %[reg4], %[reg4], %[reg5]\n\t"
                                 "add %[reg5], %[reg5], %[reg5]\n\t"
                                 "add %[reg6], %[reg6], %[reg7]\n\t"
                                 //"and %[reg7], %[reg7], %[reg7]\n\t"
                                 "fadd %s[freg2], %s[freg], %s[freg]\n\t"
                                 "fadd %s[freg3], %s[freg], %s[freg]\n\t"
                                 :[reg]"+r"(val),
                                  [reg2]"+r"(val2),
                                  [reg3]"+r"(val3),
                                  [reg4]"+r"(val4),
                                  [reg5]"+r"(val5),
                                  [reg6]"+r"(val6),
                                  [reg7]"+r"(val7),
                                  [freg]"+w"(fval),
                                  [freg2]"+w"(fval2),
                                  [freg3]"+w"(fval3),
                                  [ptr]"+r"(ptr),
                                  [ptr2]"+r"(ptr2)
                );
            );
    }
}

static void __attribute__((noinline))
load_incr(int iter)
{
    int *ptr = (int*)data_large;
    int *ptr2 = (int*)(data_large);

    int val=0;

    for (int i=0; i<100000; i++) {
        ptr[i] = ptr2[i]+1;

        ptr++;
        ptr2++;
    }

    data[0][0] = (uintptr_t)val;
    data[0][1] = (uintptr_t)ptr;
}


int main(int argc, char **argv)
{
    struct perf_event_attr attr;
    int i;
    start_usec = usec();

    memset(&attr, 0, sizeof(attr));

    attr.type = PERF_TYPE_HARDWARE;
    attr.size = sizeof(attr);
    attr.config = PERF_COUNT_HW_CPU_CYCLES;
    clk_fd = perf_event_open(&attr, 0, -1, -1, 0);
    if (clk_fd == -1) {
        perror("perf_event_open");
        exit(1);
    }

    attr.type = PERF_TYPE_HARDWARE;
    attr.size = sizeof(attr);
    attr.config = PERF_COUNT_HW_INSTRUCTIONS;
    inst_fd = perf_event_open(&attr, 0, -1, -1, 0);
    if (inst_fd == -1) {
        perror("perf_event_open");
        exit(1);
    }

    noarg_func_ptr = ret;

    int niter = 32;
    if (argc >= 2) {
        niter = atoi(argv[1]);
    }

    for (int i=0; i<4; i++) {
        test("nop", nop_func, niter);

        test("nop_call", nop_call_func, niter);
        test("unroll", unroll_func, niter);

        test("int_nop", int_nop_func, niter);
        test("int_nop_clear", int_nop_clear_func, niter);
        test("fp_nop", fp_nop_func, niter);

        test("cond-nop", cond_nop_func, niter);
        test("add1", add_func, niter);
        test("indirect_call", indirect_call, niter);
        test("load_reg1", load_reg1, niter);
        test("load_iter1", load_iter1, niter);
        test("loop_int_dep", loop_int_dep, niter);
        test("load_incr", load_incr, niter);

        test("loop_fp", loop_fp, niter);
        test("loop_fp_inc", loop_fp_inc, niter);
        test("loop_fp_dep", loop_fp_dep, niter);

        test("ipc7", ipc7, niter);
    }

    close(inst_fd);
    close(clk_fd);
}

