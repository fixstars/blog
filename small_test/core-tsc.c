#define _GNU_SOURCE

#include <x86intrin.h> 
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>

#define MAX_THREADS 256

long long core_tsc[MAX_THREADS];

struct thread_arg {
    int thread_id;
    volatile int *sync_ptr;
} thread_args[MAX_THREADS];

pthread_attr_t attrs[MAX_THREADS];
pthread_t threads[MAX_THREADS];

static void *
calib_thread(void *vp)
{
    struct thread_arg *a = (struct thread_arg*)vp;
    volatile int *p = (int*)a->sync_ptr;

    while (*p == 0)
        ;

    unsigned int id;
    unsigned long long val;

    core_tsc[a->thread_id] = __rdtsc();

    return NULL;
}

static void *
calib_thread0(void *vp)
{
    struct thread_arg *a = (struct thread_arg*)vp;
    volatile int *p = (int*)a->sync_ptr;

    usleep(1000);

    *p = 1;

    core_tsc[a->thread_id] = __rdtsc();

    return NULL;
}

int
main(void)
{
    int i;
    int syncval = 0;
    int num_thread = sysconf(_SC_NPROCESSORS_ONLN);

    if (num_thread > MAX_THREADS) {
        num_thread = MAX_THREADS;
    }

    for (i=0; i<num_thread; i++) {
        cpu_set_t set;

        CPU_ZERO(&set);
        CPU_SET(i, &set);

        pthread_attr_init(&attrs[i]);
        pthread_attr_setaffinity_np(&attrs[i], sizeof(cpu_set_t), &set);
    }

    for (i=0; i<num_thread; i++) {
        thread_args[i].sync_ptr = &syncval;
        thread_args[i].thread_id = i;
        if (i==0) {
            pthread_create(&threads[i], &attrs[i],
                           calib_thread0, &thread_args[i]);
        } else {
            pthread_create(&threads[i], &attrs[i],
                           calib_thread, &thread_args[i]);
        }
    }

    for (i=0; i<num_thread; i++) {
        pthread_join(threads[0], NULL);
        printf("%d %lld\n", i, core_tsc[i]);
    }
}
