#include <stdio.h>

int v = 1;

int __attribute__((noinline,noclone))
test0(int n)
{
    if (n == 0) {
        if (v & 1) {
            *(int*)0 = 0;
        }
    } else {
        return test0(n-1) + v;
    }

    return 10;
}

int __attribute__((noinline,noclone))
test(void)
{
    return test0(10);
}


#define N 100000
int dataA[N];
int dataB[N];
int dataC[N];

void
slow_func()
{
    int i;
    for (i=0; i<N; i++) {
        dataA[i] = dataB[i] + dataC[i];
    }
}

int
main()
{
    slow_func();

    puts("test");

    test0(10);
}
