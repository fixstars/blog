#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <x86intrin.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static int
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags )
{
    int ret;

    ret = syscall( __NR_perf_event_open, hw_event, pid, cpu,
                   group_fd, flags );
    return ret;
}

#define NUM_LOOP (1024*1024)
#define NUM_REPEAT 64
#define STRINGIZE_(A) #A
#define STRINGIZE(A) STRINGIZE_(A)

int main()
{
    struct perf_event_attr attr;
    int perf_fd;
    long long tstart, tend;
    int i;

    memset(&attr, 0, sizeof(attr));

    attr.type = PERF_TYPE_HARDWARE;
    attr.size = sizeof(attr);
    attr.config = PERF_COUNT_HW_CPU_CYCLES;

    perf_fd = perf_event_open(&attr, 0, -1, -1, 0);
    if (perf_fd == -1) {
        perror("perf_event_open");
        exit(1);
    }

#define INST                                            \
    "add %%eax, %%eax\n"                              \
    "add %%ecx, %%ecx\n"                              \

    read(perf_fd, &tstart, sizeof(tstart));
    for (i=0; i<NUM_LOOP; i++) {
        __asm__ __volatile__("1:\n\t"
                             ".rept " STRINGIZE(NUM_REPEAT) "\n\t"

                             INST

                             ".endr\n\t"
                             :
                             :
                             :"eax", "ecx", "edx", "rsi", "rdi", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15",
                              "xmm0","xmm1","xmm2","xmm3","xmm4","xmm5","xmm6","xmm7",
                              "xmm8","xmm9","xmm10","xmm11","xmm12","xmm13","xmm14","xmm15","memory");
    }
    read(perf_fd, &tend, sizeof(tend));

    puts(INST);
    printf("clk = %f\n",
           (tend-tstart)/(double)(NUM_LOOP * NUM_REPEAT));

    close(perf_fd);
}
