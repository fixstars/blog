#include <iostream>
#include <cmath>
#include <memory>
#include <chrono>

constexpr std::size_t N = 1lu << 28;
constexpr std::size_t C = 1lu << 5;
using compute_t = float;

int main() {
	auto x = std::unique_ptr<compute_t[]>(new compute_t[N]);
	auto y = std::unique_ptr<compute_t[]>(new compute_t[N]);
	auto z = std::unique_ptr<compute_t[]>(new compute_t[N]);

	for (std::size_t i = 0; i < N; i++) {
		x.get()[i] = static_cast<compute_t>(i);
		y.get()[i] = static_cast<compute_t>(N - i);
	}

	// Using *.get() in the acc kernel occurs an error "Illegal address during kernel execution"
	auto x_ptr = x.get();
	auto y_ptr = y.get();
	auto z_ptr = z.get();

	for (std::size_t L0 = 1024; L0 <= N; L0 <<= 1) {
		double elapsed_time;
#pragma acc enter data copyin(x_ptr[0:N]) copyin(y_ptr[0:N]) create(z_ptr[0:N])
		const auto start_clock = std::chrono::high_resolution_clock::now();
		for (std::size_t c = 0; c < C; c++) {
#pragma acc kernels loop independent
			for (std::size_t l0 = 0; l0 < L0; l0++) {
				const auto offset = N / L0 * l0;
				for (std::size_t i = 0; i < N / L0; i++) {
					const auto index = offset + i;
					z_ptr[index] = x_ptr[index] + y_ptr[index];
				}
			}
		}
		const auto end_clock = std::chrono::high_resolution_clock::now();
		elapsed_time = std::chrono::duration_cast<std::chrono::microseconds>(end_clock - start_clock).count() / 1.e6;
#pragma acc exit data copyout(z_ptr[0:N])

		std::printf("L0          : %lu\n", L0);
		std::printf("N           : %lu\n", N);
		std::printf("Time        : %e [s]\n", elapsed_time);
		std::printf("Performance : %e [GFlop/s]\n", N / elapsed_time / (1lu << 30) * C);
	}
}
