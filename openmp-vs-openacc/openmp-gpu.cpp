#include <iostream>
#include <cmath>
#include <memory>
#include <chrono>
#include <omp.h>

constexpr std::size_t N = 1lu << 28;
constexpr std::size_t C = 1lu << 5;
using compute_t = float;

int main() {
	auto x = std::unique_ptr<compute_t[]>(new compute_t[N]);
	auto y = std::unique_ptr<compute_t[]>(new compute_t[N]);
	auto z = std::unique_ptr<compute_t[]>(new compute_t[N]);

	for (std::size_t i = 0; i < N; i++) {
		x.get()[i] = static_cast<compute_t>(i);
		y.get()[i] = static_cast<compute_t>(N - i);
	}

	// Using *.get() in the acc kernel occurs an error "Illegal address during kernel execution"
	auto x_ptr = x.get();
	auto y_ptr = y.get();
	auto z_ptr = z.get();

	// parameter search : teams size
	for (std::size_t L0 = (1lu << 10); L0 < N; L0 <<= 1) {
#pragma omp target data map(to:x_ptr[0:N]) map(to:y_ptr[0:N]) map(from:z_ptr[0:N])
		{
			double elapsed_time;
			const auto start_clock = std::chrono::high_resolution_clock::now();
			for (unsigned c = 0; c < C; c++) {
#pragma omp target teams distribute num_teams(64)
				for (std::size_t l = 0; l < N; l += L0) {
#pragma omp parallel for
					for (std::size_t i = 0; i < L0; i++) {
						z_ptr[l + i] = x_ptr[l + i] + y_ptr[l + i];
					}
				}
			}
			const auto end_clock = std::chrono::high_resolution_clock::now();
			elapsed_time = std::chrono::duration_cast<std::chrono::microseconds>(end_clock - start_clock).count() / 1.e6;

			std::printf("L0          : %lu\n", L0);
			std::printf("N           : %lu\n", N);
			std::printf("Time        : %e [s]\n", elapsed_time);
			std::printf("Performance : %e [GFlop/s]\n", N / elapsed_time / (1lu << 30) * C);
		}
	}

	// Check
	double max_error = 0.0;
	for (std::size_t i = 0; i < N; i++) {
		max_error = std::max(std::abs(static_cast<double>(N) - z.get()[i]), max_error);
	}
	std::printf("Max error   : %e\n", max_error);
}
