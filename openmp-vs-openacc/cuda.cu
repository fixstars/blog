#include <iostream>
#include <cmath>
#include <memory>
#include <chrono>

constexpr std::size_t N = 1lu << 28;
constexpr std::size_t C = 1lu << 5;
using compute_t = float;

__global__ void vectoradd_kernel(
		compute_t* const c,
		const compute_t* const a,
		const compute_t* const b
		) {
	const auto tid = blockIdx.x * blockDim.x + threadIdx.x;

	c[tid] = a[tid] + b[tid];
}

int main() {
	auto x = std::make_unique<compute_t[]>(N);
	auto y = std::make_unique<compute_t[]>(N);
	auto z = std::make_unique<compute_t[]>(N);

	for (std::size_t i = 0; i < N; i++) {
		x.get()[i] = static_cast<compute_t>(i);
		y.get()[i] = static_cast<compute_t>(N - i);
	}

	compute_t* d_x;
	compute_t* d_y;
	compute_t* d_z;
	cudaMalloc(&d_x, sizeof(compute_t) * N);
	cudaMalloc(&d_y, sizeof(compute_t) * N);
	cudaMalloc(&d_z, sizeof(compute_t) * N);
	cudaMemcpy(d_x, x.get(), sizeof(compute_t) * N, cudaMemcpyDefault);
	cudaMemcpy(d_y, y.get(), sizeof(compute_t) * N, cudaMemcpyDefault);
	cudaMemcpy(d_z, z.get(), sizeof(compute_t) * N, cudaMemcpyDefault);

	cudaDeviceSynchronize();

	for (unsigned block_size = (1 << 5); block_size <=1024; block_size <<= 1) {
		const auto start_clock = std::chrono::high_resolution_clock::now();
		const unsigned grid_size = N / block_size;
		for (unsigned c = 0; c < C; c++) {
			vectoradd_kernel<<<grid_size, block_size>>>(d_z, d_x, d_y);
		}
		cudaDeviceSynchronize();
		const auto end_clock = std::chrono::high_resolution_clock::now();
		const auto elapsed_time = std::chrono::duration_cast<std::chrono::microseconds>(end_clock - start_clock).count() / 1.e6;

		std::printf("block_size  : %u\n", block_size);
		std::printf("N           : %lu\n", N);
		std::printf("Time        : %e [s]\n", elapsed_time);
		std::printf("Performance : %e [GFlop/s]\n", N / elapsed_time / (1lu << 30) * C);
	}
}
